# Vinland Solutions CommonCore.Drawing Library

[![License](https://commoncorelibs.gitlab.io/commoncore-drawing/badge/license.svg)](https://commoncorelibs.gitlab.io/commoncore-drawing/license.html)
[![Platform](https://commoncorelibs.gitlab.io/commoncore-drawing/badge/platform.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Repository](https://commoncorelibs.gitlab.io/commoncore-drawing/badge/repository.svg)](https://gitlab.com/commoncorelibs/commoncore-drawing)
[![Releases](https://commoncorelibs.gitlab.io/commoncore-drawing/badge/releases.svg)](https://gitlab.com/commoncorelibs/commoncore-drawing/-/releases)
[![Documentation](https://commoncorelibs.gitlab.io/commoncore-drawing/badge/documentation.svg)](https://commoncorelibs.gitlab.io/commoncore-drawing/)  
[![Nuget](https://badgen.net/nuget/v/VinlandSolutions.CommonCore.Drawing/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Drawing/)
[![Pipeline](https://gitlab.com/commoncorelibs/commoncore-drawing/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/commoncore-drawing/pipelines)
[![Coverage](https://gitlab.com/commoncorelibs/commoncore-drawing/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/commoncore-drawing/reports/index.html)
[![Tests](https://commoncorelibs.gitlab.io/commoncore-drawing/badge/tests.svg)](https://commoncorelibs.gitlab.io/commoncore-drawing/reports/index.html)

**CommonCore.Drawing** is a .Net Standard 2.0/2.1 library designed to simplify common point, size, rectangle, color, image, and drawing related operations.

## Installation

The official release versions of the **CommonCore.Drawing** library are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Drawing/) and can be installed using the standard console means, or found through the Visual Studio NuGet Package Managers.

This library is compatible with projects targeting at least .Net Framework 4.6.1, .Net Core 2.0, and .Net 5.

## Usage

TODO: Text here.

## Projects

The **CommonCore.Drawing** repository is composed of three projects with the listed dependencies:

* **CommonCore.Drawing**: The dotnet standard 2.0 library project.
  * [System.Drawing.Common](https://www.nuget.org/packages/System.Drawing.Common/)
* **CommonCore.Drawing.Docs**: The project for generating api documentation.
  * [DocFX](https://github.com/dotnet/docfx)
* **CommonCore.Drawing.Tests**: The project for running unit tests and generating coverage reports.
  * [xUnit](https://github.com/xunit/xunit)
  * [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Links

- Repository: https://gitlab.com/commoncorelibs/commoncore-drawing
- Issues: https://gitlab.com/commoncorelibs/commoncore-drawing/issues
- Docs: https://commoncorelibs.gitlab.io/commoncore-drawing/index.html
- Nuget: https://www.nuget.org/packages/VinlandSolutions.CommonCore.Drawing/

## Credits

These two libraries are used by the repo's CICD pipeline and are not a part of the library itself.

* [AnyBadge](https://github.com/jongracecox/anybadge)
* [semver](https://github.com/k-bx/python-semver)
