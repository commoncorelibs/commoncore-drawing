﻿#region Copyright (c) 2016 Jay Jeckel
// Copyright (c) 2016 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace CommonCore.Drawing
{
    /// <summary>
    /// Class to provide <see cref="Graphics"/> extension methods.
    /// </summary>
    public static class GraphicsExtensions
    {
        public static void DrawLineHorizontal(this Graphics self, Pen pen, Point point, int length)
            => self.DrawLineHorizontal(pen, point.X, point.Y, length);

        public static void DrawLineHorizontal(this Graphics self, Pen pen, int x, int y, int length)
        {
            if (self is null) { throw new NullReferenceException(); }
            else if (pen is null) { throw new ArgumentNullException(nameof(pen)); }
            else if (length < 0) { throw new ArgumentOutOfRangeException(nameof(length), $"Argument '{nameof(length)}' value '{length}' cannot be negative."); }
            else if (length >= 1) { self.DrawLine(pen, x, y, x + length - 1, y); }
        }

        public static void DrawLineHorizontal(this Graphics self, Pen pen, PointF point, int length)
            => self.DrawLineHorizontal(pen, point.X, point.Y, length);

        public static void DrawLineHorizontal(this Graphics self, Pen pen, float x, float y, float length)
        {
            if (self is null) { throw new NullReferenceException(); }
            else if (pen is null) { throw new ArgumentNullException(nameof(pen)); }
            else if (length < 0) { throw new ArgumentOutOfRangeException(nameof(length), $"Argument '{nameof(length)}' value '{length}' cannot be negative."); }
            else if (length >= 1) { self.DrawLine(pen, x, y, x + length - 1, y); }
        }

        public static void DrawLineVertical(this Graphics self, Pen pen, Point point, int length)
            => self.DrawLineVertical(pen, point.X, point.Y, length);

        public static void DrawLineVertical(this Graphics self, Pen pen, int x, int y, int length)
        {
            if (self is null) { throw new NullReferenceException(); }
            else if (pen is null) { throw new ArgumentNullException(nameof(pen)); }
            else if (length < 0) { throw new ArgumentOutOfRangeException(nameof(length), $"Argument '{nameof(length)}' value '{length}' cannot be negative."); }
            else if (length >= 1) { self.DrawLine(pen, x, y, x, y + length - 1); }
        }

        public static void DrawLineVertical(this Graphics self, Pen pen, PointF point, int length)
            => self.DrawLineVertical(pen, point.X, point.Y, length);

        public static void DrawLineVertical(this Graphics self, Pen pen, float x, float y, float length)
        {
            if (self is null) { throw new NullReferenceException(); }
            else if (pen is null) { throw new ArgumentNullException(nameof(pen)); }
            else if (length < 0) { throw new ArgumentOutOfRangeException(nameof(length), $"Argument '{nameof(length)}' value '{length}' cannot be negative."); }
            else if (length >= 1) { self.DrawLine(pen, x, y, x, y + length - 1); }
        }

        /// <summary>
        /// Draws the correct border of the specified <see cref="Rectangle"/>.
        /// The <see cref="Graphics"/> methods for drawing a rectangle treat rectangles
        /// as one larger than they are. For example, the rect <c>[0, 0, 10, 10]</c>
        /// will draw a box <c>11</c> wide and <c>11</c> high. This is because the box
        /// is drawn from <c>x</c> to <c>x + width</c>, which is <c>11</c> pixels.
        /// This method accounts for that and draws from <c>x</c> to <c>x + width - 1</c>
        /// and the same for <c>y</c> and <c>height</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="pen"><see cref="Pen"/> that determines the color, width, and style of the box.</param>
        /// <param name="rect"><see cref="Rectangle"/> that defines the box to draw.</param>
        public static void DrawBorder(this Graphics self, Pen pen, Rectangle rect)
        {
            if (rect.Width == 0 || rect.Height == 0) { return; }
            else if (rect.Width == 1 && rect.Height == 1)
            { self.DrawLine(pen, rect.X, rect.Y, rect.X, rect.Y); }
            else if (rect.Width == 1 && rect.Height > 1)
            { self.DrawLine(pen, rect.X, rect.Y, rect.X, rect.Y + rect.Height - 1); }
            else if (rect.Width > 1 && rect.Height == 1)
            { self.DrawLine(pen, rect.X, rect.Y, rect.X + rect.Width - 1, rect.Y); }
            else
            { self.DrawRectangle(pen, rect.X, rect.Y, rect.Width - 1, rect.Height - 1); }
        }

        /// <summary>
        /// Draws the correct border of the specified <see cref="RectangleF"/>.
        /// The <see cref="Graphics"/> methods for drawing a rectangle treat rectangles
        /// as one larger than they are. For example, the rect <c>[0, 0, 10, 10]</c>
        /// will draw a box <c>11</c> wide and <c>11</c> high. This is because the box
        /// is drawn from <c>x</c> to <c>x + width</c>, which is <c>11</c> pixels.
        /// This method accounts for that and draws from <c>x</c> to <c>x + width - 1</c>
        /// and the same for <c>y</c> and <c>height</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="pen"><see cref="Pen"/> that determines the color, width, and style of the box.</param>
        /// <param name="rect"><see cref="RectangleF"/> that defines the box to draw.</param>
        public static void DrawBorder(this Graphics self, Pen pen, RectangleF rect)
        {
            if (rect.Width == 0 || rect.Height == 0) { return; }
            else if (rect.Width <= 1 && rect.Height <= 1)
            { self.DrawLine(pen, rect.X, rect.Y, rect.X, rect.Y); }
            else if (rect.Width <= 1 && rect.Height > 1)
            { self.DrawLine(pen, rect.X, rect.Y, rect.X, rect.Y + rect.Height - 1); }
            else if (rect.Width > 1 && rect.Height <= 1)
            { self.DrawLine(pen, rect.X, rect.Y, rect.X + rect.Width - 1, rect.Y); }
            else
            { self.DrawRectangle(pen, rect.X, rect.Y, rect.Width - 1, rect.Height - 1); }
        }





        public static void DrawImageCentered(this Graphics self, Image image, int x, int y) => self.DrawImage(image, x - image.Width / 2, y - image.Height / 2, image.Width, image.Height);

        public static void DrawImageCentered(this Graphics self, Image image, float x, float y) => self.DrawImage(image, x - image.Width / 2, y - image.Height / 2, image.Width, image.Height);

        public static void DrawImageCentered(this Graphics self, Image image, Point point) => self.DrawImage(image, point.X - image.Width / 2, point.Y - image.Height / 2, image.Width, image.Height);

        public static void DrawImageCentered(this Graphics self, Image image, PointF point) => self.DrawImage(image, point.X - image.Width / 2, point.Y - image.Height / 2, image.Width, image.Height);
        public static void DrawImageCentered(this Graphics self, Image image, int x, int y, int width, int height) => self.DrawImage(image, x - width / 2, y - height / 2, width, height);

        public static void DrawImageCentered(this Graphics self, Image image, float x, float y, float width, float height) => self.DrawImage(image, x - width / 2, y - height / 2, width, height);

        public static void DrawImageCentered(this Graphics self, Image image, Point point, Size size) => self.DrawImage(image, point.X - size.Width / 2, point.Y - size.Height / 2, size.Width, size.Height);

        public static void DrawImageCentered(this Graphics self, Image image, PointF point, SizeF size) => self.DrawImage(image, point.X - size.Width / 2, point.Y - size.Height / 2, size.Width, size.Height);

        public static void DrawRectangleCentered(this Graphics self, Pen pen, int x, int y, int width, int height) => self.DrawRectangle(pen, x - width / 2, y - height / 2, width, height);

        public static void DrawRectangleCentered(this Graphics self, Pen pen, float x, float y, float width, float height) => self.DrawRectangle(pen, x - width / 2, y - height / 2, width, height);

        public static void DrawRectangle(this Graphics self, Pen pen, RectangleF rect) => self.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);

        /// <summary>
        /// Draws a line connecting the top-left and top-right points of the <paramref name="rect"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="pen"><see cref="Pen"/> that determines the color, width, and style of the line.</param>
        /// <param name="rect"><see cref="Rectangle"/> that defines the line to draw.</param>
        public static void DrawLineTop(this Graphics self, Pen pen, Rectangle rect) => self.DrawLine(pen, rect.Left, rect.Top, rect.Right, rect.Top);

        /// <summary>
        /// Draws a line connecting the bottom-left and bottom-right points of the <paramref name="rect"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="pen"><see cref="Pen"/> that determines the color, width, and style of the line.</param>
        /// <param name="rect"><see cref="Rectangle"/> that defines the line to draw.</param>
        public static void DrawLineBottom(this Graphics self, Pen pen, Rectangle rect) => self.DrawLine(pen, rect.Left, rect.Bottom, rect.Right, rect.Bottom);

        /// <summary>
        /// Draws a line connecting the top-left and bottom-left points of the <paramref name="rect"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="pen"><see cref="Pen"/> that determines the color, width, and style of the line.</param>
        /// <param name="rect"><see cref="Rectangle"/> that defines the line to draw.</param>
        public static void DrawLineLeft(this Graphics self, Pen pen, Rectangle rect) => self.DrawLine(pen, rect.Left, rect.Top, rect.Left, rect.Bottom);

        /// <summary>
        /// Draws a line connecting the top-right and bottom-right points of the <paramref name="rect"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="pen"><see cref="Pen"/> that determines the color, width, and style of the line.</param>
        /// <param name="rect"><see cref="Rectangle"/> that defines the line to draw.</param>
        public static void DrawLineRight(this Graphics self, Pen pen, Rectangle rect) => self.DrawLine(pen, rect.Right, rect.Top, rect.Right, rect.Bottom);









        public static Bitmap GetAdjusted(Bitmap image, ColorMatrix matrix)
        {
            var bitmap = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);
            using (var imageAttributes = new ImageAttributes())
            {
                imageAttributes.SetColorMatrix(matrix);
                using var graphics = Graphics.FromImage(bitmap);
                graphics.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributes);
            }
            return bitmap;
        }

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, int x, int y)
            => DrawImageAdjusted(self, image, matrix, new Rectangle(x, y, image.Width, image.Height), 0, 0, image.Width, image.Height);

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, Point point)
            => DrawImageAdjusted(self, image, matrix, new Rectangle(point.X, point.Y, image.Width, image.Height), 0, 0, image.Width, image.Height);

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, int x, int y, int width, int height)
            => DrawImageAdjusted(self, image, matrix, new Rectangle(x, y, width, height), 0, 0, image.Width, image.Height);

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, Point point, Size size)
            => DrawImageAdjusted(self, image, matrix, new Rectangle(point.X, point.Y, size.Width, size.Height), 0, 0, image.Width, image.Height);

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, Rectangle dest)
            => DrawImageAdjusted(self, image, matrix, dest, 0, 0, image.Width, image.Height);

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, Rectangle dest, Rectangle source)
            => DrawImageAdjusted(self, image, matrix, dest, source.X, source.Y, source.Width, source.Height);

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, Rectangle dest, RectangleF source)
            => DrawImageAdjusted(self, image, matrix, dest, source.X, source.Y, source.Width, source.Height);

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, Rectangle dest, int sourceX, int sourceY, int sourceWidth, int sourceHeight)
        {
            using var imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(matrix);
            self.DrawImage(image, dest, sourceX, sourceY, sourceWidth, sourceHeight, GraphicsUnit.Pixel, imageAttributes);
        }

        public static void DrawImageAdjusted(this Graphics self, Bitmap image, ColorMatrix matrix, Rectangle dest, float sourceX, float sourceY, float sourceWidth, float sourceHeight)
        {
            using var imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(matrix);
            self.DrawImage(image, dest, sourceX, sourceY, sourceWidth, sourceHeight, GraphicsUnit.Pixel, imageAttributes);
        }

        // DrawImageGrayscale

        public static void DrawImageGrayscale(this Graphics self, Bitmap image, int x, int y)
            => DrawImageAdjusted(self, image, ColorMatrixes.Grayscale, new Rectangle(x, y, image.Width, image.Height));

        public static void DrawImageGrayscale(this Graphics self, Bitmap image, Point point)
            => DrawImageAdjusted(self, image, ColorMatrixes.Grayscale, new Rectangle(point.X, point.Y, image.Width, image.Height));

        public static void DrawImageGrayscale(this Graphics self, Bitmap image, int x, int y, int width, int height)
            => DrawImageAdjusted(self, image, ColorMatrixes.Grayscale, new Rectangle(x, y, width, height));

        public static void DrawImageGrayscale(this Graphics self, Bitmap image, Point point, Size size)
            => DrawImageAdjusted(self, image, ColorMatrixes.Grayscale, new Rectangle(point.X, point.Y, size.Width, size.Height));

        public static void DrawImageGrayscale(this Graphics self, Bitmap image, Rectangle dest)
            => DrawImageAdjusted(self, image, ColorMatrixes.Grayscale, dest);

        public static void DrawImageGrayscale(this Graphics self, Bitmap image, Rectangle dest, Rectangle source)
            => DrawImageAdjusted(self, image, ColorMatrixes.Grayscale, dest, source.X, source.Y, source.Width, source.Height);

        public static void DrawImageGrayscale(this Graphics self, Bitmap image, Rectangle dest, RectangleF source)
            => DrawImageAdjusted(self, image, ColorMatrixes.Grayscale, dest, source.X, source.Y, source.Width, source.Height);

        // DrawImageNegative

        public static void DrawImageNegative(this Graphics self, Bitmap image, int x, int y)
            => DrawImageAdjusted(self, image, ColorMatrixes.Negative, new Rectangle(x, y, image.Width, image.Height));

        public static void DrawImageNegative(this Graphics self, Bitmap image, Point point)
            => DrawImageAdjusted(self, image, ColorMatrixes.Negative, new Rectangle(point.X, point.Y, image.Width, image.Height));

        public static void DrawImageNegative(this Graphics self, Bitmap image, int x, int y, int width, int height)
            => DrawImageAdjusted(self, image, ColorMatrixes.Negative, new Rectangle(x, y, width, height));

        public static void DrawImageNegative(this Graphics self, Bitmap image, Point point, Size size)
            => DrawImageAdjusted(self, image, ColorMatrixes.Negative, new Rectangle(point.X, point.Y, size.Width, size.Height));

        public static void DrawImageNegative(this Graphics self, Bitmap image, Rectangle dest)
            => DrawImageAdjusted(self, image, ColorMatrixes.Negative, dest);

        public static void DrawImageNegative(this Graphics self, Bitmap image, Rectangle dest, Rectangle source)
            => DrawImageAdjusted(self, image, ColorMatrixes.Negative, dest, source.X, source.Y, source.Width, source.Height);

        public static void DrawImageNegative(this Graphics self, Bitmap image, Rectangle dest, RectangleF source)
            => DrawImageAdjusted(self, image, ColorMatrixes.Negative, dest, source.X, source.Y, source.Width, source.Height);

        // DrawImageSepia

        public static void DrawImageSepia(this Graphics self, Bitmap image, int x, int y)
            => DrawImageAdjusted(self, image, ColorMatrixes.Sepia, new Rectangle(x, y, image.Width, image.Height));

        public static void DrawImageSepia(this Graphics self, Bitmap image, Point point)
            => DrawImageAdjusted(self, image, ColorMatrixes.Sepia, new Rectangle(point.X, point.Y, image.Width, image.Height));

        public static void DrawImageSepia(this Graphics self, Bitmap image, int x, int y, int width, int height)
            => DrawImageAdjusted(self, image, ColorMatrixes.Sepia, new Rectangle(x, y, width, height));

        public static void DrawImageSepia(this Graphics self, Bitmap image, Point point, Size size)
            => DrawImageAdjusted(self, image, ColorMatrixes.Sepia, new Rectangle(point.X, point.Y, size.Width, size.Height));

        public static void DrawImageSepia(this Graphics self, Bitmap image, Rectangle dest)
            => DrawImageAdjusted(self, image, ColorMatrixes.Sepia, dest);

        public static void DrawImageSepia(this Graphics self, Bitmap image, Rectangle dest, Rectangle source)
            => DrawImageAdjusted(self, image, ColorMatrixes.Sepia, dest, source.X, source.Y, source.Width, source.Height);

        public static void DrawImageSepia(this Graphics self, Bitmap image, Rectangle dest, RectangleF source)
            => DrawImageAdjusted(self, image, ColorMatrixes.Sepia, dest, source.X, source.Y, source.Width, source.Height);
    }
}
