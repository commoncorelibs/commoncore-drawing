﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    public static partial class PointExtensions
    {
        public static Point WithX(this Point self, int x)
            => new(x, self.Y);

        public static Point WithX(this Point self, Point point) => self.WithX(point.X);

        public static Point WithX(this Point self, Rectangle rect) => self.WithX(rect.X);



        public static Point WithY(this Point self, int y)
            => new(self.X, y);

        public static Point WithY(this Point self, Point point) => self.WithY(point.Y);

        public static Point WithY(this Point self, Rectangle rect) => self.WithY(rect.Y);



        public static Point WithDelta(this Point self, int xDelta, int yDelta)
            => new(self.X + xDelta, self.Y + yDelta);

        public static Point WithDelta(this Point self, Point point) => self.WithDelta(point.X, point.Y);

        public static Point WithDelta(this Point self, Size size) => self.WithDelta(size.Width, size.Height);



        public static Point WithDeltaX(this Point self, int xDelta)
            => new(self.X + xDelta, self.Y);

        public static Point WithDeltaX(this Point self, Point point) => self.WithDeltaX(point.X);

        public static Point WithDeltaX(this Point self, Size size) => self.WithDeltaX(size.Width);



        public static Point WithDeltaY(this Point self, int yDelta)
            => new(self.X, self.Y + yDelta);

        public static Point WithDeltaY(this Point self, Point point) => self.WithDeltaY(point.Y);

        public static Point WithDeltaY(this Point self, Size size) => self.WithDeltaY(size.Height);
    }
}
