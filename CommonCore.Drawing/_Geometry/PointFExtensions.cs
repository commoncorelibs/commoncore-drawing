﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Drawing;

namespace CommonCore.Drawing
{
    /// <summary>
    /// Class to provide <see cref="System.Drawing.PointF" /> extension methods.
    /// </summary>
    public static partial class PointFExtensions
    {
        public static Point ToPoint(this PointF self)
            => new((int) self.X, (int) self.Y);

        /// <inheritdoc cref="Point.Ceiling(PointF)"/>
        public static Point ToCeiling(this PointF self)
            => Point.Ceiling(self);

        /// <inheritdoc cref="Point.Truncate(PointF)"/>
        public static Point ToFloor(this PointF self)
            => Point.Truncate(self);

        /// <inheritdoc cref="Point.Round(PointF)"/>
        public static Point ToRound(this PointF self)
            => Point.Round(self);
    }
}
