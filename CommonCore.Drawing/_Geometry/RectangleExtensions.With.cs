﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    public static partial class RectangleExtensions
    {
        public static Rectangle WithPoint(this Rectangle self, int x, int y)
            => new(x, y, self.Width, self.Height);

        public static Rectangle WithPoint(this Rectangle self, Point point) => self.WithPoint(point.X, point.Y);

        public static Rectangle WithPoint(this Rectangle self, Rectangle rect) => self.WithPoint(rect.X, rect.Y);



        public static Rectangle WithX(this Rectangle self, int x)
            => new(x, self.Y, self.Width, self.Height);

        public static Rectangle WithX(this Rectangle self, Point point) => self.WithX(point.X);

        public static Rectangle WithX(this Rectangle self, Rectangle rect) => self.WithX(rect.X);



        public static Rectangle WithY(this Rectangle self, int y)
            => new(self.X, y, self.Width, self.Height);

        public static Rectangle WithY(this Rectangle self, Point point) => self.WithY(point.Y);

        public static Rectangle WithY(this Rectangle self, Rectangle rect) => self.WithY(rect.Y);



        public static Rectangle WithSize(this Rectangle self, int width, int height)
            => new(self.X, self.Y, width, height);

        public static Rectangle WithSize(this Rectangle self, Size size) => self.WithSize(size.Width, size.Height);

        public static Rectangle WithSize(this Rectangle self, Rectangle rect) => self.WithSize(rect.Width, rect.Height);



        public static Rectangle WithWidth(this Rectangle self, int width)
            => new(self.X, self.Y, width, self.Height);

        public static Rectangle WithWidth(this Rectangle self, Size size) => self.WithWidth(size.Width);

        public static Rectangle WithWidth(this Rectangle self, Rectangle rect) => self.WithWidth(rect.Width);



        public static Rectangle WithHeight(this Rectangle self, int height)
            => new(self.X, self.Y, self.Width, height);

        public static Rectangle WithHeight(this Rectangle self, Size size) => self.WithHeight(size.Height);

        public static Rectangle WithHeight(this Rectangle self, Rectangle rect) => self.WithHeight(rect.Height);



        public static Rectangle WithDelta(this Rectangle self, int xDelta, int yDelta, int widthDelta, int heightDelta)
            => new(self.X + xDelta, self.Y + yDelta, self.Width + widthDelta, self.Height + heightDelta);

        public static Rectangle WithDelta(this Rectangle self, Point point, Size size)
            => new(self.X + point.X, self.Y + point.Y, self.Width + size.Width, self.Height + size.Height);

        public static Rectangle WithDelta(this Rectangle self, Rectangle rect)
            => new(self.X + rect.X, self.Y + rect.Y, self.Width + rect.Width, self.Height + rect.Height);



        public static Rectangle WithDeltaPoint(this Rectangle self, int xDelta, int yDelta)
            => new(self.X + xDelta, self.Y + yDelta, self.Width, self.Height);

        public static Rectangle WithDeltaPoint(this Rectangle self, Point point) => self.WithDeltaPoint(point.X, point.Y);

        public static Rectangle WithDeltaPoint(this Rectangle self, Size size) => self.WithDeltaPoint(size.Width, size.Height);



        public static Rectangle WithDeltaX(this Rectangle self, int xDelta)
            => new(self.X + xDelta, self.Y, self.Width, self.Height);

        public static Rectangle WithDeltaX(this Rectangle self, Point point) => self.WithDeltaX(point.X);

        public static Rectangle WithDeltaX(this Rectangle self, Size size) => self.WithDeltaX(size.Width);



        public static Rectangle WithDeltaY(this Rectangle self, int yDelta)
            => new(self.X, self.Y + yDelta, self.Width, self.Height);

        public static Rectangle WithDeltaY(this Rectangle self, Point point) => self.WithDeltaY(point.Y);

        public static Rectangle WithDeltaY(this Rectangle self, Size size) => self.WithDeltaY(size.Height);



        public static Rectangle WithDeltaSize(this Rectangle self, int widthDelta, int heightDelta)
            => new(self.X, self.Y, self.Width + widthDelta, self.Height + heightDelta);

        public static Rectangle WithDeltaSize(this Rectangle self, Point point) => self.WithDeltaSize(point.X, point.Y);

        public static Rectangle WithDeltaSize(this Rectangle self, Size size) => self.WithDeltaSize(size.Width, size.Height);



        public static Rectangle WithDeltaWidth(this Rectangle self, int widthDelta)
            => new(self.X, self.Y, self.Width + widthDelta, self.Height);

        public static Rectangle WithDeltaWidth(this Rectangle self, Point point) => self.WithDeltaWidth(point.X);

        public static Rectangle WithDeltaWidth(this Rectangle self, Size size) => self.WithDeltaWidth(size.Width);



        public static Rectangle WithDeltaHeight(this Rectangle self, int heightDelta)
            => new(self.X, self.Y, self.Width, self.Height + heightDelta);

        public static Rectangle WithDeltaHeight(this Rectangle self, Point point) => self.WithDeltaHeight(point.Y);

        public static Rectangle WithDeltaHeight(this Rectangle self, Size size) => self.WithDeltaHeight(size.Height);
    }
}
