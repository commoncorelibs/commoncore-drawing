﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    /// <summary>
    /// Class to provide <see cref="System.Drawing.Rectangle" /> extension methods.
    /// </summary>
    public static partial class RectangleExtensions
    {
        public static RectangleF ToRectF(this Rectangle self)
            => new(self.X, self.Y, self.Width, self.Height);

        /// <summary>
        /// Gets the <see cref="int"/> center <c>x</c> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="int"/> center <c>x</c> of the <see cref="Rectangle"/>.</returns>
        public static int ToCenterX(this Rectangle self)
            => self.X + (self.Width / 2);

        /// <summary>
        /// Gets the <see cref="int"/> center <c>y</c> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="int"/> center <c>y</c> of the <see cref="Rectangle"/>.</returns>
        public static int ToCenterY(this Rectangle self)
            => self.Y + (self.Height / 2);

        /// <summary>
        /// Gets the center <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The center <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToCenter(this Rectangle self)
            => new(self.ToCenterX(), self.ToCenterY());

        /// <summary>
        /// Gets the <see cref="float"/> center <c>x</c> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="float"/> center <c>x</c> of the <see cref="Rectangle"/>.</returns>
        public static float ToCenterXF(this Rectangle self)
            => self.X + (self.Width / 2f);

        /// <summary>
        /// Gets the <see cref="float"/> center <c>y</c> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="float"/> center <c>y</c> of the <see cref="Rectangle"/>.</returns>
        public static float ToCenterYF(this Rectangle self)
            => self.Y + (self.Height / 2f);

        /// <summary>
        /// Gets the center <see cref="PointF"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The center <see cref="PointF"/> of the <see cref="Rectangle"/>.</returns>
        public static PointF ToCenterF(this Rectangle self)
            => new(self.ToCenterXF(), self.ToCenterYF());

        /// <summary>
        /// Gets the <see cref="int"/> left <c>x</c> of the <see cref="Rectangle"/>.
        /// Simply returns the <see cref="Rectangle.X"/> value unaltered.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="int"/> left <c>x</c> of the <see cref="Rectangle"/>.</returns>
        public static int ToLeftX(this Rectangle self)
            => self.X;

        /// <summary>
        /// Gets the <see cref="int"/> right <c>x</c> of the <see cref="Rectangle"/>.
        /// Returns the correct value for the right <c>x</c> using the formula
        /// <c>right = x + (w == 0 ? 0 : w - 1)</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="int"/> right <c>x</c> of the <see cref="Rectangle"/>.</returns>
        public static int ToRightX(this Rectangle self)
            => self.Width == 0 ? self.X : self.X + self.Width - 1;

        /// <summary>
        /// Gets the <see cref="int"/> top <c>y</c> of the <see cref="Rectangle"/>.
        /// Simply returns the <see cref="Rectangle.Y"/> value unaltered.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="int"/> top <c>y</c> of the <see cref="Rectangle"/>.</returns>
        public static int ToTopY(this Rectangle self)
            => self.Y;

        /// <summary>
        /// Gets the <see cref="int"/> bottom <c>y</c> of the <see cref="Rectangle"/>.
        /// Returns the correct value for the bottom <c>y</c> using the formula
        /// <c>bottom = y + (h == 0 ? 0 : h - 1)</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="int"/> bottom <c>y</c> of the <see cref="Rectangle"/>.</returns>
        public static int ToBottomY(this Rectangle self)
            => self.Height == 0 ? self.Y : self.Y + self.Height - 1;

        /// <summary>
        /// Gets the left top <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The left top <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToLeftTop(this Rectangle self)
            => new(self.X, self.Y);

        /// <summary>
        /// Gets the left center <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The left center <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToLeftCenter(this Rectangle self)
            => new(self.X, self.ToCenterY());

        /// <summary>
        /// Gets the left bottom <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The left bottom <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToLeftBottom(this Rectangle self)
            => new(self.X, self.ToBottomY());

        /// <summary>
        /// Gets the center top <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The center top <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToCenterTop(this Rectangle self)
            => new(self.ToCenterX(), self.Y);

        /// <summary>
        /// Gets the center bottom <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The center bottom <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToCenterBottom(this Rectangle self)
            => new(self.ToCenterX(), self.ToBottomY());

        /// <summary>
        /// Gets the right top <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The right top <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToRightTop(this Rectangle self)
            => new(self.ToRightX(), self.Y);

        /// <summary>
        /// Gets the right center <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The right center <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToRightCenter(this Rectangle self)
            => new(self.ToRightX(), self.ToCenterY());

        /// <summary>
        /// Gets the right bottom <see cref="Point"/> of the <see cref="Rectangle"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The right bottom <see cref="Point"/> of the <see cref="Rectangle"/>.</returns>
        public static Point ToRightBottom(this Rectangle self)
            => new(self.ToRightX(), self.ToBottomY());

        /// <summary>
        /// Gets a copy of the <see cref="Rectangle"/> inflated by the specified values.
        /// The <c>X</c> will be decreased by the <paramref name="horizontal"/> value
        /// and the <c>Width</c> will be increased by twice the <paramref name="horizontal"/> value;
        /// likewise the <c>Y</c> and <c>Height</c> with the <paramref name="vertical"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="horizontal">Amount to inflate the <c>X</c> and <c>Width</c>.</param>
        /// <param name="vertical">Amount to inflate the <c>Y</c> and <c>Height</c>.</param>
        /// <returns>A copy of the <see cref="Rectangle"/> inflated by the specified values.</returns>
        public static Rectangle ToInflated(this Rectangle self, int horizontal, int vertical)
            => new(self.X - horizontal, self.Y - vertical, self.Width + (2 * horizontal), self.Height + (2 * vertical));

        /// <summary>
        /// Gets a copy of the <see cref="Rectangle"/> deflated by the specified values.
        /// The <c>X</c> will be increased by the <paramref name="horizontal"/> value
        /// and the <c>Width</c> will be decreased by twice the <paramref name="horizontal"/> value;
        /// likewise the <c>Y</c> and <c>Height</c> with the <paramref name="vertical"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="horizontal">Amount to deflate the <c>X</c> and <c>Width</c>.</param>
        /// <param name="vertical">Amount to deflate the <c>Y</c> and <c>Height</c>.</param>
        /// <returns>A copy of the <see cref="Rectangle"/> deflated by the specified values.</returns>
        public static Rectangle ToDeflated(this Rectangle self, int horizontal, int vertical)
            => new(self.X + horizontal, self.Y + vertical, self.Width - (2 * horizontal), self.Height - (2 * vertical));
    }
}
