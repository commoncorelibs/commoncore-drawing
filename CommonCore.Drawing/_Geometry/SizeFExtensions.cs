﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    /// <summary>
    /// Class to provide <see cref="System.Drawing.SizeF" /> extension methods.
    /// </summary>
    public static partial class SizeFExtensions
    {
        /// <inheritdoc cref="Size.Ceiling(SizeF)"/>
        public static Size ToCeiling(this SizeF self)
            => Size.Ceiling(self);

        /// <inheritdoc cref="Size.Truncate(SizeF)"/>
        public static Size ToFloor(this SizeF self)
            => Size.Truncate(self);

        /// <inheritdoc cref="Size.Round(SizeF)"/>
        public static Size ToRound(this SizeF self)
            => Size.Round(self);
    }
}
