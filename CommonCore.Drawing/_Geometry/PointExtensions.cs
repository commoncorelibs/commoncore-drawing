﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    /// <summary>
    /// Class to provide <see cref="System.Drawing.Point" /> extension methods.
    /// </summary>
    public static partial class PointExtensions
    {
        public static PointF ToPointF(this Point self)
            => new(self.X, self.Y);
    }
}
