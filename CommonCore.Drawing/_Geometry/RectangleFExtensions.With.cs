﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    public static partial class RectangleFExtensions
    {
        public static RectangleF WithPoint(this RectangleF self, float x, float y)
            => new(x, y, self.Width, self.Height);

        public static RectangleF WithPoint(this RectangleF self, Point point) => self.WithPoint(point.X, point.Y);

        public static RectangleF WithPoint(this RectangleF self, PointF pointF) => self.WithPoint(pointF.X, pointF.Y);

        public static RectangleF WithPoint(this RectangleF self, Rectangle rect) => self.WithPoint(rect.X, rect.Y);

        public static RectangleF WithPoint(this RectangleF self, RectangleF rectF) => self.WithPoint(rectF.X, rectF.Y);



        public static RectangleF WithX(this RectangleF self, float x)
            => new(x, self.Y, self.Width, self.Height);

        public static RectangleF WithX(this RectangleF self, Point point) => self.WithX(point.X);

        public static RectangleF WithX(this RectangleF self, PointF pointF) => self.WithX(pointF.X);

        public static RectangleF WithX(this RectangleF self, Rectangle rect) => self.WithX(rect.X);

        public static RectangleF WithX(this RectangleF self, RectangleF rectF) => self.WithX(rectF.X);



        public static RectangleF WithY(this RectangleF self, float y)
            => new(self.X, y, self.Width, self.Height);

        public static RectangleF WithY(this RectangleF self, Point point) => self.WithY(point.Y);

        public static RectangleF WithY(this RectangleF self, PointF pointF) => self.WithY(pointF.Y);

        public static RectangleF WithY(this RectangleF self, Rectangle rect) => self.WithY(rect.Y);

        public static RectangleF WithY(this RectangleF self, RectangleF rectF) => self.WithY(rectF.Y);



        public static RectangleF WithSize(this RectangleF self, float width, float height)
            => new(self.X, self.Y, width, height);

        public static RectangleF WithSize(this RectangleF self, Size size) => self.WithSize(size.Width, size.Height);

        public static RectangleF WithSize(this RectangleF self, SizeF sizeF) => self.WithSize(sizeF.Width, sizeF.Height);

        public static RectangleF WithSize(this RectangleF self, Rectangle rect) => self.WithSize(rect.Width, rect.Height);

        public static RectangleF WithSize(this RectangleF self, RectangleF rectF) => self.WithSize(rectF.Width, rectF.Height);



        public static RectangleF WithWidth(this RectangleF self, float width)
            => new(self.X, self.Y, width, self.Height);

        public static RectangleF WithWidth(this RectangleF self, Size size) => self.WithWidth(size.Width);

        public static RectangleF WithWidth(this RectangleF self, SizeF sizeF) => self.WithWidth(sizeF.Width);

        public static RectangleF WithWidth(this RectangleF self, Rectangle rect) => self.WithWidth(rect.Width);

        public static RectangleF WithWidth(this RectangleF self, RectangleF rectF) => self.WithWidth(rectF.Width);



        public static RectangleF WithHeight(this RectangleF self, float height)
            => new(self.X, self.Y, self.Width, height);

        public static RectangleF WithHeight(this RectangleF self, Size size) => self.WithHeight(size.Height);

        public static RectangleF WithHeight(this RectangleF self, SizeF sizeF) => self.WithHeight(sizeF.Height);

        public static RectangleF WithHeight(this RectangleF self, Rectangle rect) => self.WithHeight(rect.Height);

        public static RectangleF WithHeight(this RectangleF self, RectangleF rectF) => self.WithHeight(rectF.Height);



        public static RectangleF WithDelta(this RectangleF self, float xDelta, float yDelta, float widthDelta, float heightDelta)
            => new(self.X + xDelta, self.Y + yDelta, self.Width + widthDelta, self.Height + heightDelta);

        public static RectangleF WithDelta(this RectangleF self, Point point, Size size)
            => new(self.X + point.X, self.Y + point.Y, self.Width + size.Width, self.Height + size.Height);

        public static RectangleF WithDelta(this RectangleF self, PointF point, SizeF size)
            => new(self.X + point.X, self.Y + point.Y, self.Width + size.Width, self.Height + size.Height);

        public static RectangleF WithDelta(this RectangleF self, Rectangle rect)
            => new(self.X + rect.X, self.Y + rect.Y, self.Width + rect.Width, self.Height + rect.Height);

        public static RectangleF WithDelta(this RectangleF self, RectangleF rect)
            => new(self.X + rect.X, self.Y + rect.Y, self.Width + rect.Width, self.Height + rect.Height);



        public static RectangleF WithDeltaPoint(this RectangleF self, float xDelta, float yDelta)
            => new(self.X + xDelta, self.Y + yDelta, self.Width, self.Height);

        public static RectangleF WithDeltaPoint(this RectangleF self, Point point) => self.WithDeltaPoint(point.X, point.Y);

        public static RectangleF WithDeltaPoint(this RectangleF self, PointF point) => self.WithDeltaPoint(point.X, point.Y);

        public static RectangleF WithDeltaPoint(this RectangleF self, Size size) => self.WithDeltaPoint(size.Width, size.Height);

        public static RectangleF WithDeltaPoint(this RectangleF self, SizeF size) => self.WithDeltaPoint(size.Width, size.Height);



        public static RectangleF WithDeltaX(this RectangleF self, float xDelta)
            => new(self.X + xDelta, self.Y, self.Width, self.Height);

        public static RectangleF WithDeltaX(this RectangleF self, Point point) => self.WithDeltaX(point.X);

        public static RectangleF WithDeltaX(this RectangleF self, PointF point) => self.WithDeltaX(point.X);

        public static RectangleF WithDeltaX(this RectangleF self, Size size) => self.WithDeltaX(size.Width);

        public static RectangleF WithDeltaX(this RectangleF self, SizeF size) => self.WithDeltaX(size.Width);



        public static RectangleF WithDeltaY(this RectangleF self, float yDelta)
            => new(self.X, self.Y + yDelta, self.Width, self.Height);

        public static RectangleF WithDeltaY(this RectangleF self, Point point) => self.WithDeltaY(point.Y);

        public static RectangleF WithDeltaY(this RectangleF self, PointF point) => self.WithDeltaY(point.Y);

        public static RectangleF WithDeltaY(this RectangleF self, Size size) => self.WithDeltaY(size.Height);

        public static RectangleF WithDeltaY(this RectangleF self, SizeF size) => self.WithDeltaY(size.Height);



        public static RectangleF WithDeltaSize(this RectangleF self, float widthDelta, float heightDelta)
            => new(self.X, self.Y, self.Width + widthDelta, self.Height + heightDelta);

        public static RectangleF WithDeltaSize(this RectangleF self, Point point) => self.WithDeltaSize(point.X, point.Y);

        public static RectangleF WithDeltaSize(this RectangleF self, PointF point) => self.WithDeltaSize(point.X, point.Y);

        public static RectangleF WithDeltaSize(this RectangleF self, Size size) => self.WithDeltaSize(size.Width, size.Height);

        public static RectangleF WithDeltaSize(this RectangleF self, SizeF size) => self.WithDeltaSize(size.Width, size.Height);



        public static RectangleF WithDeltaWidth(this RectangleF self, float widthDelta)
            => new(self.X, self.Y, self.Width + widthDelta, self.Height);

        public static RectangleF WithDeltaWidth(this RectangleF self, Point point) => self.WithDeltaWidth(point.X);

        public static RectangleF WithDeltaWidth(this RectangleF self, PointF point) => self.WithDeltaWidth(point.X);

        public static RectangleF WithDeltaWidth(this RectangleF self, Size size) => self.WithDeltaWidth(size.Width);

        public static RectangleF WithDeltaWidth(this RectangleF self, SizeF size) => self.WithDeltaWidth(size.Width);



        public static RectangleF WithDeltaHeight(this RectangleF self, float heightDelta)
            => new(self.X, self.Y, self.Width, self.Height + heightDelta);

        public static RectangleF WithDeltaHeight(this RectangleF self, Point point) => self.WithDeltaHeight(point.Y);

        public static RectangleF WithDeltaHeight(this RectangleF self, PointF point) => self.WithDeltaHeight(point.Y);

        public static RectangleF WithDeltaHeight(this RectangleF self, Size size) => self.WithDeltaHeight(size.Height);

        public static RectangleF WithDeltaHeight(this RectangleF self, SizeF size) => self.WithDeltaHeight(size.Height);
    }
}
