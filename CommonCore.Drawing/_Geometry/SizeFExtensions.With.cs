﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    public static partial class SizeFExtensions
    {
        public static SizeF WithWidth(this SizeF self, float width)
            => new(width, self.Height);

        public static SizeF WithWidth(this SizeF self, Size size) => self.WithWidth(size.Width);

        public static SizeF WithWidth(this SizeF self, SizeF size) => self.WithWidth(size.Width);

        public static SizeF WithWidth(this SizeF self, Rectangle rect) => self.WithWidth(rect.Width);

        public static SizeF WithWidth(this SizeF self, RectangleF rectF) => self.WithWidth(rectF.Width);



        public static SizeF WithHeight(this SizeF self, float height)
            => new(self.Width, height);

        public static SizeF WithHeight(this SizeF self, Size size) => self.WithHeight(size.Height);

        public static SizeF WithHeight(this SizeF self, SizeF size) => self.WithHeight(size.Height);

        public static SizeF WithHeight(this SizeF self, Rectangle rect) => self.WithHeight(rect.Height);

        public static SizeF WithHeight(this SizeF self, RectangleF rectF) => self.WithHeight(rectF.Height);



        public static SizeF WithDelta(this SizeF self, float widthDelta, float heightDelta)
            => new(self.Width + widthDelta, self.Height + heightDelta);

        public static SizeF WithDelta(this SizeF self, Point point) => self.WithDelta(point.X, point.Y);

        public static SizeF WithDelta(this SizeF self, PointF point) => self.WithDelta(point.X, point.Y);

        public static SizeF WithDelta(this SizeF self, Size size) => self.WithDelta(size.Width, size.Height);

        public static SizeF WithDelta(this SizeF self, SizeF size) => self.WithDelta(size.Width, size.Height);



        public static SizeF WithDeltaWidth(this SizeF self, float widthDelta)
            => new(self.Width + widthDelta, self.Height);

        public static SizeF WithDeltaWidth(this SizeF self, Point point) => self.WithDeltaWidth(point.X);

        public static SizeF WithDeltaWidth(this SizeF self, PointF point) => self.WithDeltaWidth(point.X);

        public static SizeF WithDeltaWidth(this SizeF self, Size size) => self.WithDeltaWidth(size.Width);

        public static SizeF WithDeltaWidth(this SizeF self, SizeF size) => self.WithDeltaWidth(size.Width);



        public static SizeF WithDeltaHeight(this SizeF self, float heightDelta)
            => new(self.Width, self.Height + heightDelta);

        public static SizeF WithDeltaHeight(this SizeF self, Point point) => self.WithDeltaHeight(point.Y);

        public static SizeF WithDeltaHeight(this SizeF self, PointF point) => self.WithDeltaHeight(point.Y);

        public static SizeF WithDeltaHeight(this SizeF self, Size size) => self.WithDeltaHeight(size.Height);

        public static SizeF WithDeltaHeight(this SizeF self, SizeF size) => self.WithDeltaHeight(size.Height);
    }
}
