﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    /// <summary>
    /// Class to provide <see cref="System.Drawing.RectangleF" /> extension methods.
    /// </summary>
    public static partial class RectangleFExtensions
    {
        public static Rectangle ToRect(this RectangleF self)
            => new((int) self.X, (int) self.Y, (int) self.Width, (int) self.Height);

        /// <inheritdoc cref="Rectangle.Ceiling(RectangleF)"/>
        public static Rectangle ToCeiling(this RectangleF self)
            => Rectangle.Ceiling(self);

        /// <inheritdoc cref="Rectangle.Truncate(RectangleF)"/>
        public static Rectangle ToFloor(this RectangleF self)
            => Rectangle.Truncate(self);

        /// <inheritdoc cref="Rectangle.Round(RectangleF)"/>
        public static Rectangle ToRound(this RectangleF self)
            => Rectangle.Round(self);

        /// <summary>
        /// Gets the center <c>x</c> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The center <c>x</c> of the <see cref="RectangleF"/>.</returns>
        public static float ToCenterX(this RectangleF self)
            => self.X + (self.Width / 2);

        /// <summary>
        /// Gets the center <c>y</c> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The center <c>y</c> of the <see cref="RectangleF"/>.</returns>
        public static float ToCenterY(this RectangleF self)
            => self.Y + (self.Height / 2);

        /// <summary>
        /// Gets the center <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Center <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToCenter(this RectangleF self)
            => new(self.ToCenterX(), self.ToCenterY());

        /// <summary>
        /// Gets the left <c>x</c> of the <see cref="RectangleF"/>.
        /// Simply returns the <see cref="RectangleF.X"/> value unaltered.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The left <c>x</c> of the <see cref="RectangleF"/>.</returns>
        public static float ToLeftX(this RectangleF self)
            => self.X;

        /// <summary>
        /// Gets the <see cref="int"/> right <c>x</c> of the <see cref="RectangleF"/>.
        /// Returns the correct value for the right <c>x</c> using the formula
        /// <c>right = x + (w &lt; 1 ? 0 : w - 1)</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="int"/> right <c>x</c> of the <see cref="RectangleF"/>.</returns>
        public static float ToRightX(this RectangleF self)
            => self.Width < 1 ? self.X : self.X + self.Width - 1;

        /// <summary>
        /// Gets the top <c>y</c> of the <see cref="RectangleF"/>.
        /// Simply returns the <see cref="RectangleF.Y"/> value unaltered.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The top <c>y</c> of the <see cref="RectangleF"/>.</returns>
        public static float ToTopY(this RectangleF self)
            => self.Y;

        /// <summary>
        /// Gets the <see cref="int"/> bottom <c>y</c> of the <see cref="RectangleF"/>.
        /// Returns the correct value for the bottom <c>y</c> using the formula
        /// <c>bottom = y + (h &lt; 1 ? 0 : h - 1)</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The <see cref="int"/> bottom <c>y</c> of the <see cref="RectangleF"/>.</returns>
        public static float ToBottomY(this RectangleF self)
            => self.Height < 1 ? self.Y : self.Y + self.Height - 1;

        /// <summary>
        /// Gets the left top <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The left top <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToLeftTop(this RectangleF self)
            => new(self.X, self.Y);

        /// <summary>
        /// Gets the left center <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The left center <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToLeftCenter(this RectangleF self)
            => new(self.X, self.ToCenterY());

        /// <summary>
        /// Gets the left bottom <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The left bottom <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToLeftBottom(this RectangleF self)
            => new(self.X, self.ToBottomY());

        /// <summary>
        /// Gets the center top <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The center top <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToCenterTop(this RectangleF self)
            => new(self.ToCenterX(), self.Y);

        /// <summary>
        /// Gets the center bottom <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The center bottom <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToCenterBottom(this RectangleF self)
            => new(self.ToCenterX(), self.ToBottomY());

        /// <summary>
        /// Gets the right top <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The right top <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToRightTop(this RectangleF self)
            => new(self.ToRightX(), self.Y);

        /// <summary>
        /// Gets the right center <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The right center <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToRightCenter(this RectangleF self)
            => new(self.ToRightX(), self.ToCenterY());

        /// <summary>
        /// Gets the right bottom <see cref="PointF"/> of the <see cref="RectangleF"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The right bottom <see cref="PointF"/> of the <see cref="RectangleF"/>.</returns>
        public static PointF ToRightBottom(this RectangleF self)
            => new(self.ToRightX(), self.ToBottomY());

        /// <summary>
        /// Gets a copy of the <see cref="RectangleF"/> inflated by the specified values.
        /// The <c>X</c> will be decreased by the <paramref name="horizontal"/> value
        /// and the <c>Width</c> will be increased by twice the <paramref name="horizontal"/> value;
        /// likewise the <c>Y</c> and <c>Height</c> with the <paramref name="vertical"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="horizontal">Amount to inflate the <c>X</c> and <c>Width</c>.</param>
        /// <param name="vertical">Amount to inflate the <c>Y</c> and <c>Height</c>.</param>
        /// <returns>A copy of the <see cref="RectangleF"/> inflated by the specified values.</returns>
        public static RectangleF ToInflated(this RectangleF self, float horizontal, float vertical)
            => new(self.X - horizontal, self.Y - vertical, self.Width + (2 * horizontal), self.Height + (2 * vertical));

        /// <summary>
        /// Gets a copy of the <see cref="RectangleF"/> deflated by the specified values.
        /// The <c>X</c> will be increased by the <paramref name="horizontal"/> value
        /// and the <c>Width</c> will be decreased by twice the <paramref name="horizontal"/> value;
        /// likewise the <c>Y</c> and <c>Height</c> with the <paramref name="vertical"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="horizontal">Amount to deflate the <c>X</c> and <c>Width</c>.</param>
        /// <param name="vertical">Amount to deflate the <c>Y</c> and <c>Height</c>.</param>
        /// <returns>A copy of the <see cref="RectangleF"/> deflated by the specified values.</returns>
        public static RectangleF ToDeflated(this RectangleF self, float horizontal, float vertical)
            => new(self.X + horizontal, self.Y + vertical, self.Width - (2 * horizontal), self.Height - (2 * vertical));
    }
}
