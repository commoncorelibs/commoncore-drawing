﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    public static partial class PointFExtensions
    {
        public static PointF WithX(this PointF self, float x)
            => new(x, self.Y);

        public static PointF WithX(this PointF self, Point point) => self.WithX(point.X);

        public static PointF WithX(this PointF self, PointF pointF) => self.WithX(pointF.X);

        public static PointF WithX(this PointF self, Rectangle rect) => self.WithX(rect.X);

        public static PointF WithX(this PointF self, RectangleF rectF) => self.WithX(rectF.X);



        public static PointF WithY(this PointF self, float y)
            => new(self.X, y);

        public static PointF WithY(this PointF self, Point point) => self.WithY(point.Y);

        public static PointF WithY(this PointF self, PointF pointF) => self.WithY(pointF.Y);

        public static PointF WithY(this PointF self, Rectangle rect) => self.WithY(rect.Y);

        public static PointF WithY(this PointF self, RectangleF rectF) => self.WithY(rectF.Y);



        public static PointF WithDelta(this PointF self, float xDelta, float yDelta)
            => new(self.X + xDelta, self.Y + yDelta);

        public static PointF WithDelta(this PointF self, Point delta) => self.WithDelta(delta.X, delta.Y);

        public static PointF WithDelta(this PointF self, PointF delta) => self.WithDelta(delta.X, delta.Y);

        public static PointF WithDelta(this PointF self, Size delta) => self.WithDelta(delta.Width, delta.Height);

        public static PointF WithDelta(this PointF self, SizeF delta) => self.WithDelta(delta.Width, delta.Height);



        public static PointF WithDeltaX(this PointF self, float xDelta)
            => new(self.X + xDelta, self.Y);

        public static PointF WithDeltaX(this PointF self, Point delta) => self.WithDeltaX(delta.X);

        public static PointF WithDeltaX(this PointF self, PointF delta) => self.WithDeltaX(delta.X);

        public static PointF WithDeltaX(this PointF self, Size delta) => self.WithDeltaX(delta.Width);

        public static PointF WithDeltaX(this PointF self, SizeF delta) => self.WithDeltaX(delta.Width);



        public static PointF WithDeltaY(this PointF self, float yDelta)
            => new(self.X, self.Y + yDelta);

        public static PointF WithDeltaY(this PointF self, Point delta) => self.WithDeltaY(delta.Y);

        public static PointF WithDeltaY(this PointF self, PointF delta) => self.WithDeltaY(delta.Y);

        public static PointF WithDeltaY(this PointF self, Size delta) => self.WithDeltaY(delta.Height);

        public static PointF WithDeltaY(this PointF self, SizeF delta) => self.WithDeltaY(delta.Height);
    }
}
