﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    public static partial class SizeExtensions
    {
        public static Size WithWidth(this Size self, int width)
            => new(width, self.Height);

        public static Size WithWidth(this Size self, Size size) => self.WithWidth(size.Width);

        public static Size WithWidth(this Size self, Rectangle rect) => self.WithWidth(rect.Width);



        public static Size WithHeight(this Size self, int height)
            => new(self.Width, height);

        public static Size WithHeight(this Size self, Size size) => self.WithHeight(size.Height);

        public static Size WithHeight(this Size self, Rectangle rect) => self.WithHeight(rect.Height);



        public static Size WithDelta(this Size self, int widthDelta, int heightDelta)
            => new(self.Width + widthDelta, self.Height + heightDelta);

        public static Size WithDelta(this Size self, Point point) => self.WithDelta(point.X, point.Y);

        public static Size WithDelta(this Size self, Size size) => self.WithDelta(size.Width, size.Height);



        public static Size WithDeltaWidth(this Size self, int widthDelta)
            => new(self.Width + widthDelta, self.Height);

        public static Size WithDeltaWidth(this Size self, Point point) => self.WithDeltaWidth(point.X);

        public static Size WithDeltaWidth(this Size self, Size size) => self.WithDeltaWidth(size.Width);



        public static Size WithDeltaHeight(this Size self, int heightDelta)
            => new(self.Width, self.Height + heightDelta);

        public static Size WithDeltaHeight(this Size self, Point point) => self.WithDeltaHeight(point.Y);

        public static Size WithDeltaHeight(this Size self, Size size) => self.WithDeltaHeight(size.Height);
    }
}
