#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Drawing;

namespace CommonCore.Drawing
{
    /// <summary>
    /// The <see cref="ColorExtensions"/> <c>static</c> <c>extension</c> <c>class</c> provides
    /// extension methods related to <see cref="Color"/> objects.
    /// </summary>
    public static partial class ColorExtensions
    {
        /// <summary>
        /// Deconstructs the <see cref="Color"/> into red, green, and blue values.
        /// Allows deconstructing of <see cref="Color"/> objects to <c>rgb</c> tuples.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="r">The red channel.</param>
        /// <param name="g">The green channel.</param>
        /// <param name="b">The blue channel.</param>
        public static void Deconstruct(this Color self, out byte r, out byte g, out byte b)
            => (r, g, b) = (self.R, self.G, self.B);

        /// <summary>
        /// Deconstructs the <see cref="Color"/> into red, green, and blue values.
        /// Allows deconstructing of <see cref="Color"/> objects to <c>argb</c> tuples.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="a">The alpha channel.</param>
        /// <param name="r">The red channel.</param>
        /// <param name="g">The green channel.</param>
        /// <param name="b">The blue channel.</param>
        public static void Deconstruct(this Color self, out byte a, out byte r, out byte g, out byte b)
            => (a, r, g, b) = (self.A, self.R, self.G, self.B);

        /// <summary>
        /// Determines whether the <see cref="Color"/> is opaque,
        /// ie has an alpha value equal to <c>255</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        public static bool IsOpaque(this Color self) => self.A == 255;

        /// <summary>
        /// Determines whether the <see cref="Color"/> is invisible,
        /// ie has an alpha value equal to <c>0</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns></returns>
        public static bool IsInvisible(this Color self) => self.A == 0;

        /// <summary>
        /// Determines whether the <see cref="Color"/> is transparent,
        /// ie has an alpha value less than <c>255</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        public static bool IsTransparent(this Color self) => self.A < 255;

        /// <summary>
        /// Determines whether the <see cref="Color"/> is translucent,
        /// ie has an alpha value greater than <c>0</c> and less than <c>255</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        public static bool IsTranslucent(this Color self) => self.A is > 0 and < 255;

        /// <summary>
        /// Creates a <see cref="Bitmap"/> filled with the <see cref="Color"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="width">Width of the bitmap.</param>
        /// <param name="height">Height of the bitmap.</param>
        /// <returns>A <see cref="Bitmap"/> filled with the <see cref="Color"/>.</returns>
        public static Bitmap ToBitmap(this Color self, int width = 1, int height = 1)
        {
            if (width < 1) { throw new ArgumentOutOfRangeException(nameof(width), $"Argument {nameof(width)} value '{width}' must be greater than '0'."); }
            if (height < 1) { throw new ArgumentOutOfRangeException(nameof(height), $"Argument {nameof(height)} value '{height}' must be greater than '0'."); }

            var bitmap = new Bitmap(width, height);
            using var graphics = Graphics.FromImage(bitmap);
            using var solidBrush = new SolidBrush(self);
            graphics.FillRectangle(solidBrush, new Rectangle(0, 0, width, height));
            return bitmap;
        }

        /// <summary>
        /// Converts the <see cref="Color"/> to a hex <see cref="string"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="format">The pattern to use in conversion to hex. 0 is alpha, 1 is red, 2 is green, and 3 is blue.</param>
        /// <returns>The <see cref="Color"/> as a hex <see cref="string"/>.</returns>
        public static string ToHex(this Color self, string? format = "#{0:X2}{1:X2}{2:X2}{3:X2}")
            => string.Format(format ?? "#{0:X2}{1:X2}{2:X2}{3:X2}", self.A, self.R, self.G, self.B);

        /// <summary>
        /// Gets the inverted value of the <see cref="Color"/>.
        /// By default, the alpha channel is not inverted, though this
        /// behavior can be controlled with the <paramref name="invertAlpha"/>
        /// parameter.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="invertAlpha">Whether the alpha channel is inverted.</param>
        /// <returns>Inverted value of the <see cref="Color"/>.</returns>
        public static Color ToInverted(this Color self, bool invertAlpha = false)
            => Color.FromArgb(invertAlpha ? (byte) (~self.A) : self.A, (byte) ~self.R, (byte) ~self.G, (byte) ~self.B);

        /// <summary>
        /// Gets the desaturated value of the <see cref="Color"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Desaturated value of the <see cref="Color"/>.</returns>
        /// <remarks>A NTSC weighted average is used to desaturate the input color.</remarks>
        public static Color ToDesaturated(this Color self)
            => Color.FromArgb(self.A, Color.FromArgb(-0x010101 * (255 - (int) (self.R * 0.3 + self.G * 0.6 + self.B * 0.1)) - 1));

        /// <summary>
        /// Gets a value of the <see cref="Color"/> with the brightness altered.
        /// Specify a positive <paramref name="percent"/> value to lighten the color
        /// or a negative value to darken the color.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="percent">Unit percentage (-1 to 1, inclusive) to alter the brightness.</param>
        /// <returns>A value of the <see cref="Color"/> with the brightness altered.</returns>
        public static Color ToBrightnessChanged(this Color self, double percent)
            => ColorHelper.ChangeBrightness(self, percent);

        /// <summary>
        /// Gets a lighter value of the <see cref="Color"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="percent">The unit percent (0 to 1) to lighten the color.</param>
        /// <returns>A lighter value of the <see cref="Color"/>.</returns>
        public static Color ToLightened(this Color self, double percent)
            => ColorHelper.Lighten(self, percent);

        /// <summary>
        /// Gets a darker value of the <see cref="Color"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="percent">The unit percent (0 to 1) to darken the color.</param>
        /// <returns>A darker value of the <see cref="Color"/>.</returns>
        public static Color ToDarkened(this Color self, double percent)
            => ColorHelper.Darken(self, percent);
    }
}
