#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Drawing;
using System.Globalization;

namespace CommonCore.Drawing
{
    /// <summary>
    /// The <see cref="ColorHelper"/> <c>static</c> <c>helper</c> <c>class</c> provides
    /// methods related to <see cref="Color"/> objects.
    /// </summary>
    public static class ColorHelper
    {
        /// <summary>
        /// Attempts to convert the specified hex encoded <see cref="string"/> to a <see cref="Color"/>.
        /// Returns <c>true</c> if conversion was successful; otherwise false;
        /// </summary>
        /// <param name="hex">The string to attempt conversion.</param>
        /// <param name="color">The converted color is successful.</param>
        /// <returns><c>true</c> if the conversion was successful; otherwise <c>false</c>.</returns>
        public static bool TryParseHex(string hex, out Color color)
        {
            if (int.TryParse(hex is null ? string.Empty : hex.Replace("#", ""), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out int argb))
            {
                color = Color.FromArgb(argb);
                return true;
            }
            color = default;
            return false;
        }

        /// <summary>
        /// Gets a value of the <see cref="Color"/> with the brightness altered.
        /// Specify a positive <paramref name="percent"/> value to lighten the color
        /// or a negative value to darken the color.
        /// </summary>
        /// <param name="color">The <see cref="Color"/> to alter.</param>
        /// <param name="percent">Unit percentage (-1 to 1, inclusive) to alter the brightness.</param>
        /// <returns>A value of the <see cref="Color"/> with the brightness altered.</returns>
        public static Color ChangeBrightness(Color color, double percent)
            => percent is < -1 or > 1
                ? throw new ArgumentOutOfRangeException(nameof(percent), $"Argument '{nameof(percent)}' must be within the inclusive range '-1.0' to '1.0'.")
                : percent == 0
                ? color
                : percent > 0f
                ? ColorHelper.Lighten(color, percent)
                : ColorHelper.Darken(color, -percent);

        /// <summary>
        /// Gets a lighter value of the <see cref="Color"/>.
        /// </summary>
        /// <param name="color">The <see cref="Color"/> to lighten.</param>
        /// <param name="percent">The unit percent (0 to 1) to lighten the color.</param>
        /// <returns>A lighter value of the <see cref="Color"/>.</returns>
        public static Color Lighten(Color color, double percent)
            => percent is < 0 or > 1
                ? throw new ArgumentOutOfRangeException(nameof(percent), $"Argument '{nameof(percent)}' must be within the inclusive range '0.0' to '1.0'.")
                : percent == 0
                ? color
                : Color.FromArgb(color.A,
                    (int) (~color.R * percent + color.R),
                    (int) (~color.G * percent + color.G),
                    (int) (~color.B * percent + color.B));

        /// <summary>
        /// Gets a darker value of the <see cref="Color"/>.
        /// </summary>
        /// <param name="color">The <see cref="Color"/> to darken.</param>
        /// <param name="percent">The unit percent (0 to 1) to darken the color.</param>
        /// <returns>A darker value of the <see cref="Color"/>.</returns>
        public static Color Darken(Color color, double percent)
        {
            if (percent == 0) { return color; }
            percent = 1f + -percent;
            return percent is < 0 or > 1
                ? throw new ArgumentOutOfRangeException(nameof(percent), $"Argument '{nameof(percent)}' must be within the inclusive range '0.0' to '1.0'.")
                : Color.FromArgb(color.A,
                    (int) (color.R * percent),
                    (int) (color.G * percent),
                    (int) (color.B * percent));
        }
    }
}
