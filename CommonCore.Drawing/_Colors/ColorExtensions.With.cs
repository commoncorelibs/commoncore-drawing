﻿#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Drawing;

namespace CommonCore.Drawing
{
    public static partial class ColorExtensions
    {
        public static Color WithA(this Color self, byte a)
            => Color.FromArgb(a, self.R, self.G, self.B);

        public static Color WithR(this Color self, byte r)
            => Color.FromArgb(self.A, r, self.G, self.B);

        public static Color WithG(this Color self, byte g)
            => Color.FromArgb(self.A, self.R, g, self.B);

        public static Color WithB(this Color self, byte b)
            => Color.FromArgb(self.A, self.R, self.G, b);

        public static Color WithA(this Color self, int a)
            => Color.FromArgb(a, self.R, self.G, self.B);

        public static Color WithR(this Color self, int r)
            => Color.FromArgb(self.A, r, self.G, self.B);

        public static Color WithG(this Color self, int g)
            => Color.FromArgb(self.A, self.R, g, self.B);

        public static Color WithB(this Color self, int b)
            => Color.FromArgb(self.A, self.R, self.G, b);

        public static Color WithA(this Color self, Color color)
            => Color.FromArgb(color.A, self.R, self.G, self.B);

        public static Color WithR(this Color self, Color color)
            => Color.FromArgb(self.A, color.R, self.G, self.B);

        public static Color WithG(this Color self, Color color)
            => Color.FromArgb(self.A, self.R, color.G, self.B);

        public static Color WithB(this Color self, Color color)
            => Color.FromArgb(self.A, self.R, self.G, color.B);

        public static Color WithDelta(this Color self, int r, int g, int b)
            => Color.FromArgb(self.A, self.R + r, self.G + g, self.B + b);

        public static Color WithDelta(this Color self, int a, int r, int g, int b)
            => Color.FromArgb(self.A + a, self.R + r, self.G + g, self.B + b);

        public static Color WithDeltaA(this Color self, int a)
            => Color.FromArgb(self.A + a, self.R, self.G, self.B);

        public static Color WithDeltaR(this Color self, int r)
            => Color.FromArgb(self.A, self.R + r, self.G, self.B);

        public static Color WithDeltaG(this Color self, int g)
            => Color.FromArgb(self.A, self.R, self.G + g, self.B);

        public static Color WithDeltaB(this Color self, int b)
            => Color.FromArgb(self.A, self.R, self.G, self.B + b);
    }
}
