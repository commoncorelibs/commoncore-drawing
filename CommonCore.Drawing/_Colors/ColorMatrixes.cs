﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Drawing.Imaging;

namespace CommonCore.Drawing
{
    /// <summary>
    /// The <see cref="ColorMatrixes"/> <c>static</c> <c>class</c>
    /// contains <see cref="ColorMatrix"/> objects used by various
    /// image manipulation operations.
    /// </summary>
    public static class ColorMatrixes
    {
        /// <summary>
        /// Gets the <see cref="ColorMatrix"/> used with image grayscale operations.
        /// </summary>
        public static ColorMatrix Grayscale { get; } = new(new float[][]
            {
                new float[] { 0.299F, 0.299F, 0.299F, 0, 0 },
                new float[] { 0.587F, 0.587F, 0.587F, 0, 0 },
                new float[] { 0.114F, 0.114F, 0.114F, 0, 0 },
                new float[] { 0, 0, 0, 1, 0 },
                new float[] { 0, 0, 0, 0, 1 }
            });

        /// <summary>
        /// Gets the <see cref="ColorMatrix"/> used with image negative operations.
        /// </summary>
        public static ColorMatrix Negative { get; } = new(new float[][]
            {
                new float[] { -1, 0, 0, 0, 0 },
                new float[] { 0, -1, 0, 0, 0 },
                new float[] { 0, 0, -1, 0, 0 },
                new float[] { 0, 0, 0, 1, 0 },
                new float[] { 1, 1, 1, 0, 1 }
            });

        /// <summary>
        /// Gets the <see cref="ColorMatrix"/> used with image sepia operations.
        /// </summary>
        public static ColorMatrix Sepia { get; } = new(new float[][]
            {
                new float[] { 0.393f, 0.349f, 0.272f, 0, 0 },
                new float[] { 0.769f, 0.686f, 0.534f, 0, 0 },
                new float[] { 0.189f, 0.168f, 0.131f, 0, 0 },
                new float[] { 0, 0, 0, 1, 0 },
                new float[] { 0, 0, 0, 0, 1 }
            });

        /// <summary>
        /// Gets the <see cref="ColorMatrix"/> used with image shadow operations.
        /// </summary>
        public static ColorMatrix Shadow { get; } = new(new float[][]
            {
                new float[] { 0, 0, 0, 0, 0 },
                new float[] { 0, 0, 0, 0, 0 },
                new float[] { 0, 0, 0, 0, 0 },
                new float[] { 0, 0, 0, 0.25f, 0 },
                new float[] { 0, 0, 0, 0, 1 }
            });
    }
}
