﻿#region Copyright (c) 2016 Jay Jeckel
// Copyright (c) 2016 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;

namespace CommonCore.Drawing
{
    /// <summary>
    /// The <see cref="ColorPair"/> <c>readonly</c> <c>struct</c> represents
    /// a pair of <see cref="Color"/> values.
    /// </summary>
    public readonly struct ColorPair : IEquatable<ColorPair>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ColorPair"/> struct.
        /// </summary>
        /// <param name="a">First <see cref="Color"/> of the <see cref="ColorPair"/>.</param>
        /// <param name="b">Second <see cref="Color"/> of the <see cref="ColorPair"/>.</param>
        public ColorPair(Color a, Color b)
        {
            this.A = a;
            this.B = b;
        }

        /// <summary>
        /// Gets the first <see cref="Color"/> of the <see cref="ColorPair"/>.
        /// </summary>
        public readonly Color A { get; }

        /// <summary>
        /// Gets the second <see cref="Color"/> of the <see cref="ColorPair"/>.
        /// </summary>
        public readonly Color B { get; }

        /// <summary>
        /// Gets a new <see cref="ColorPair"/> object with the <see cref="Color"/> values swapped
        /// so that the first color becomes the second and the second color becomes the first.
        /// </summary>
        /// <returns>A new <see cref="ColorPair"/> object with the <see cref="Color"/> values swapped.</returns>
        public readonly ColorPair ToSwapped() => new(this.B, this.A);

        /// <summary>
        /// Returns a <see cref="string"/> representation of the <see cref="ColorPair"/>.
        /// </summary>
        /// <returns>A <see cref="string"/> representation of the <see cref="ColorPair"/>.</returns>
        public override readonly string ToString() => $"[{this.A}, {this.B}]";

        /// <summary>
        /// Gets the hashcode of the <see cref="ColorPair"/>.
        /// </summary>
        /// <returns>The hashcode of the <see cref="ColorPair"/>.</returns>
        [ExcludeFromCodeCoverage]
        public override readonly int GetHashCode()
        {
            int hashCode = -1817952719;
            hashCode = hashCode * -1521134295 + this.A.GetHashCode();
            hashCode = hashCode * -1521134295 + this.B.GetHashCode();
            return hashCode;
        }

        /// <summary>
        /// Determines if the <paramref name="obj"/> is a <see cref="ColorPair"/>
        /// and the two <see cref="ColorPair"/> objects are equal.
        /// </summary>
        /// <param name="obj">The <see cref="object"/> to compare.</param>
        /// <returns><c>true</c> if the <paramref name="obj"/> is a <see cref="ColorPair"/> and the two <see cref="ColorPair"/> objects are equal; otherwise <c>false</c>.</returns>
        public override readonly bool Equals(object? obj)
            => obj is ColorPair pair && this.Equals(pair);

        /// <summary>
        /// Determines if the <see cref="ColorPair"/> is equal to <paramref name="other"/>.
        /// </summary>
        /// <param name="other">The <see cref="ColorPair"/> to compare.</param>
        /// <returns><c>true</c> if the two <see cref="ColorPair"/> objects are equal; otherwise <c>false</c>.</returns>
        public readonly bool Equals(ColorPair other)
            => EqualityComparer<Color>.Default.Equals(this.A, other.A)
            && EqualityComparer<Color>.Default.Equals(this.B, other.B);

        /// <summary>
        /// Compare two <see cref="ColorPair"/> objects for equality.
        /// </summary>
        /// <param name="left">The left <see cref="ColorPair"/> object.</param>
        /// <param name="right">The right <see cref="ColorPair"/> object.</param>
        /// <returns><c>true</c> if the two <see cref="ColorPair"/> objects are equal; otherwise <c>false</c>.</returns>
        public static bool operator ==(ColorPair left, ColorPair right) => left.Equals(right);

        /// <summary>
        /// Compare two <see cref="ColorPair"/> objects for inequality.
        /// </summary>
        /// <param name="left">The left <see cref="ColorPair"/> object.</param>
        /// <param name="right">The right <see cref="ColorPair"/> object.</param>
        /// <returns><c>true</c> if the two <see cref="ColorPair"/> objects are not equal; otherwise <c>false</c>.</returns>
        public static bool operator !=(ColorPair left, ColorPair right) => !left.Equals(right);
    }
}
