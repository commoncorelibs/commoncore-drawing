#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace CommonCore.Drawing
{
    /// <summary>
    /// The <see cref="ImageExtensions"/> <c>static</c> <c>extension</c> <c>class</c> provides
    /// extension methods related to <see cref="Image"/> objects.
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        /// Get image as an Icon.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Image as an Icon.</returns>
        public static Icon ToIcon(this Image self)
            => self is null
                ? throw new System.NullReferenceException()
                : self is Bitmap bitmap
                ? Icon.FromHandle(bitmap.GetHicon())
                : throw new System.ArgumentException("The 'Image' must be castable to 'Bitmap'.");

        /// <summary>
        /// Get a scaled version of the image.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="size">Size to scale the image.</param>
        /// <returns>Scaled version of the image.</returns>
        public static Image Scale(this Image self, Size size)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetScaled((Bitmap)self, size);

        /// <summary>
        /// Get version of the image with given colors swapped.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="colors">List of color pairs to swap.</param>
        /// <returns>Version of the image with given colors swapped.</returns>
        public static Image Swap(this Image self, params ColorPair[] colors)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetSwapped((Bitmap)self, colors);

        /// <summary>
        /// Get a cropped version of the given image.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="x">X position to start the new cropped image.</param>
        /// <param name="y">Y position to start the new cropped image.</param>
        /// <param name="width">Width of the new cropped image.</param>
        /// <param name="height">Height of the new cropped image.</param>
        /// <returns>Cropped version of the given image.</returns>
        public static Image Crop(this Image self, int x, int y, int width, int height)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetCropped((Bitmap)self, x, y, width, height);

        /// <summary>
        /// Get image with color matrix applied.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="matrix">Color matrix to apply.</param>
        /// <returns>Image with color matrix applied.</returns>
        public static Image Adjust(this Image self, ColorMatrix matrix)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetAdjusted((Bitmap)self, matrix);

        /// <summary>
        /// Get a grayscale version of the image.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Grayscale version of the image.</returns>
        public static Image Grayscale(this Image self)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetAdjusted((Bitmap)self, ColorMatrixes.Grayscale);

        /// <summary>
        /// Get a negative version of the image.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Negative version of the image.</returns>
        public static Image Negative(this Image self)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetAdjusted((Bitmap)self, ColorMatrixes.Negative);

        /// <summary>
        /// Get a sepia version of the bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Sepia version of the image.</returns>
        public static Image Sepia(this Image self)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetAdjusted((Bitmap)self, ColorMatrixes.Sepia);

        /// <summary>
        /// Get a shadow version of the bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Shadow version of the image.</returns>
        public static Image Shadow(this Image self)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetAdjusted((Bitmap)self, ColorMatrixes.Shadow);

        /// <summary>
        /// Get a tinted version of the image.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="color">Color to use for tinting.</param>
        /// <param name="intensity">Intensity of tinting. Value is currently ignored.</param>
        /// <returns>Tinted version of the image.</returns>
        public static Image Tint(this Image self, Color color, float intensity = 1f)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetTint((Bitmap)self, color, intensity);

        /// <summary>
        /// Get a version of the image with a changed transparency.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="intensity">Intensity of transparency.</param>
        /// <returns>Version of the bitmap with a changed transparency.</returns>
        public static Image Transparency(this Image self, float intensity)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetTransparency((Bitmap)self, intensity);

        /// <summary>
        /// Get new image containing the specified image and its reflection.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="backColor">Color to fill the background.</param>
        /// <param name="reflectivity">Amount to reflect.</param>
        /// <returns>Image with the image and its reflection.</returns>
        public static Image Reflection(this Image self, Color backColor, int reflectivity)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetReflection((Bitmap)self, backColor, reflectivity);

        public static Image Rotate(this Image self, float angleDegrees)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetRotated((Bitmap)self, angleDegrees);

        public static Image Square(this Image self)
            => self is null
                ? throw new NullReferenceException()
                : BitmapHelper.GetSquare((Bitmap)self);
    }
}
