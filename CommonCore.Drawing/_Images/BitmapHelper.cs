﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace CommonCore.Drawing
{
    /// <summary>
    /// The <see cref="BitmapHelper"/> <c>static</c> <c>helper</c> <c>class</c> provides
    /// methods related to <see cref="Bitmap"/> and <see cref="Image"/> objects.
    /// </summary>
    public static class BitmapHelper
    {
        ///// <summary>
        ///// Get bitmap as an Icon.
        ///// </summary>
        ///// <param name="image">Bitmap to get icon from.</param>
        ///// <returns>Bitmap as an Icon.</returns>
        //public static Icon GetIcon(Bitmap image)
        //    => Icon.FromHandle(image.GetHicon());

        /// <summary>
        /// Get a scaled version of the bitmap.
        /// </summary>
        /// <param name="image">Bitmap to scale.</param>
        /// <param name="size">Size to scale the bitmap.</param>
        /// <returns>Scaled version of the bitmap.</returns>
        /// TODO: Possible better scaling code: http://www.codeproject.com/Articles/11143/Image-Resizing-outperform-GDI
        public static Bitmap GetScaled(Bitmap image, Size size)
        {
            var bitmap = new Bitmap(size.Width, size.Height);
            bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            using var graphics = Graphics.FromImage(bitmap);
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.DrawImage(image, 0, 0, bitmap.Width, bitmap.Height);
            return bitmap;
        }

        /// <summary>
        /// Get version of the bitmap with given colors swapped.
        /// </summary>
        /// <param name="image">Bitmap to swap colors within.</param>
        /// <param name="colors">List of color pairs to swap.</param>
        /// <returns>Version of the bitmap with given colors swapped.</returns>
        public static Bitmap GetSwapped(Bitmap image, params ColorPair[] colors)
        {
            var bitmap = new Bitmap(image.Width, image.Height);
            using var graphics = Graphics.FromImage(bitmap);

            var map = new ColorMap[colors.Length];

            for (int index = 0; index < colors.Length; index++)
            {
                map[index] = new ColorMap()
                {
                    OldColor = colors[index].A,
                    NewColor = colors[index].B
                };
            }

            using var imageAttributes = new ImageAttributes();
            imageAttributes.SetRemapTable(map);
            graphics.DrawImage(image,
                new Rectangle(0, 0, image.Width, image.Height),
                0, 0, image.Width, image.Height,
                GraphicsUnit.Pixel, imageAttributes);
            return bitmap;
        }

        /// <summary>
        /// Get a cropped version of the given bitmap.
        /// </summary>
        /// <param name="image">Bitmap to crop.</param>
        /// <param name="x">X position to start the new cropped image.</param>
        /// <param name="y">Y position to start the new cropped image.</param>
        /// <param name="width">Width of the new cropped image.</param>
        /// <param name="height">Height of the new cropped image.</param>
        /// <returns>Cropped version of the given bitmap.</returns>
        public static Bitmap GetCropped(Bitmap image, int x, int y, int width, int height)
        {
            if (x + width > image.Width) { width = image.Width - x; }
            if (y + height > image.Height) { height = image.Height - y; }
            var rect = new Rectangle(x, y, width, height);
            return (Bitmap)image.Clone(rect, image.PixelFormat);
        }

        /// <summary>
        /// Get bitmap with color matrix applied.
        /// </summary>
        /// <param name="image">Bitmap to adjust.</param>
        /// <param name="matrix">Color matrix to apply.</param>
        /// <returns>Bitmap with color matrix applied.</returns>
        public static Bitmap GetAdjusted(Bitmap image, ColorMatrix matrix)
        {
            var bitmap = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);
            using var imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(matrix);
            using var graphics = Graphics.FromImage(bitmap);
            graphics.DrawImage(image,
                new Rectangle(0, 0, image.Width, image.Height),
                0, 0, image.Width, image.Height,
                GraphicsUnit.Pixel, imageAttributes);
            return bitmap;
        }

        /// <summary>
        /// Get a grayscale version of the bitmap.
        /// </summary>
        /// <param name="image">Bitmap to adjust to grayscale.</param>
        /// <returns>Grayscale version of the bitmap.</returns>
        public static Bitmap GetGrayscale(Bitmap image)
            => BitmapHelper.GetAdjusted(image, ColorMatrixes.Grayscale);

        /// <summary>
        /// Get a negative version of the bitmap.
        /// </summary>
        /// <param name="image">Bitmap to adjust to negative.</param>
        /// <returns>Negative version of the bitmap.</returns>
        public static Bitmap GetNegative(Bitmap image)
            => BitmapHelper.GetAdjusted(image, ColorMatrixes.Negative);

        /// <summary>
        /// Get a sepia version of the bitmap.
        /// </summary>
        /// <param name="image">Bitmap to adjust to sepia.</param>
        /// <returns>Sepia version of the bitmap.</returns>
        public static Bitmap GetSepia(Bitmap image)
            => BitmapHelper.GetAdjusted(image, ColorMatrixes.Sepia);

        /// <summary>
        /// Get a shadow version of the bitmap.
        /// </summary>
        /// <param name="image">Bitmap to adjust to shadow.</param>
        /// <returns>Shadow version of the bitmap.</returns>
        public static Bitmap GetShadow(Bitmap image)
            => BitmapHelper.GetAdjusted(image, ColorMatrixes.Shadow);

        /// <summary>
        /// Get a tinted version of the bitmap.
        /// </summary>
        /// <param name="image">Bitmap to adjust to tint.</param>
        /// <param name="color">Color to use for tinting.</param>
        /// <param name="intensity">Intensity of tinting.</param>
        /// <returns>Tinted version of the bitmap.</returns>
        public static Bitmap GetTint(Bitmap image, Color color, double intensity = 1.0F)
        {
            if (intensity is < (-1.0) or > 1.0) { throw new ArgumentOutOfRangeException("intensity", "Argument must range between -1.0 and 1.0, inclusive."); }
            float[][] array = new float[][]
            {
                new float[] { (float) intensity, 0, 0, 0, 0 },
                new float[] { 0, (float) intensity, 0, 0, 0 },
                new float[] { 0, 0, (float) intensity, 0, 0 },
                new float[] { 0, 0, 0, 1, 0 },
                new float[] { color.R / 255.0F, color.G / 255.0F, color.B / 255.0F, 0F, 1F }
            };
            return BitmapHelper.GetAdjusted(image, new ColorMatrix(array));
        }

        /// <summary>
        /// Get a version of the bitmap with a changed transparency.
        /// </summary>
        /// <param name="image">Bitmap to adjust to transparency.</param>
        /// <param name="intensity">Intensity of transparency.</param>
        /// <returns>Version of the bitmap with a changed transparency.</returns>
        public static Bitmap GetTransparency(Bitmap image, double intensity)
        {
            if (intensity is < (-1.0) or > 1.0) { throw new ArgumentOutOfRangeException("intensity", "Argument must range between -1.0 and 1.0, inclusive."); }
            float[][] array = new float[][]
            {
                new float[] { 1, 0, 0, 0, 0 },
                new float[] { 0, 1, 0, 0, 0 },
                new float[] { 0, 0, 1, 0, 0 },
                new float[] { 0, 0, 0, (float) intensity, 0 },
                new float[] { 0, 0, 0, 0, 1 }
            };
            return BitmapHelper.GetAdjusted(image, new ColorMatrix(array));
        }

        /// <summary>
        /// Get new bitmap containing the specified bitmap and its reflection.
        /// </summary>
        /// <param name="image">Bitmap to reflect.</param>
        /// <param name="backColor">Color to fill the background.</param>
        /// <param name="reflectivity">Amount to reflect.</param>
        /// <returns>Bitmap with the bitmap and its reflection.</returns>
        public static Bitmap GetReflection(Bitmap image, Color backColor, int reflectivity)
        {
            int height = (int) (image.Height + image.Height * (reflectivity / 255f));
            var bitmap = new Bitmap(image.Width, height, PixelFormat.Format24bppRgb);
            bitmap.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            using var graphics = Graphics.FromImage(bitmap);
            graphics.Clear(backColor);
            graphics.DrawImage(image, new Point(0, 0));
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            var rectangle = new Rectangle(0, image.Size.Height, image.Size.Width, image.Size.Height);
            int height2 = image.Height * reflectivity / 255;
            using var bitmap2 = new Bitmap(image.Width, height2);
            using (var graphics2 = Graphics.FromImage(bitmap2))
            {
                graphics2.DrawImage(image, new Rectangle(0, 0, bitmap2.Width, bitmap2.Height), 0, image.Height - bitmap2.Height, bitmap2.Width, bitmap2.Height, GraphicsUnit.Pixel);
            }
            bitmap2.RotateFlip(RotateFlipType.Rotate180FlipX);
            var rect = new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height * reflectivity / 255);
            graphics.DrawImage(bitmap2, rect);
            using var brush = new LinearGradientBrush(rect, Color.FromArgb(255 - reflectivity, backColor), backColor, 90f, false);
            graphics.FillRectangle(brush, rect);
            return bitmap;
        }

        public static Bitmap GetRotated(Bitmap image, float angleDegrees)
        {
            int length = (int) Math.Ceiling(Math.Sqrt(image.Width * image.Width + image.Height * image.Height));
            var bitmap = new Bitmap(length, length);
            using var graphics = Graphics.FromImage(bitmap);
            graphics.TranslateTransform((float) bitmap.Width / 2, (float) bitmap.Height / 2);
            graphics.RotateTransform(angleDegrees);
            graphics.TranslateTransform(-(float) bitmap.Width / 2, -(float) bitmap.Height / 2);
            graphics.DrawImage(image,
                bitmap.Width / 2 - image.Width / 2,
                bitmap.Height / 2 - image.Height / 2,
                image.Width, image.Height);
            return bitmap;

            //////create a new empty bitmap to hold rotated image
            ////Bitmap returnBitmap = new Bitmap(b.Width, b.Height);
            //////make a graphics object from the empty bitmap
            ////using (Graphics g = Graphics.FromImage(returnBitmap))
            ////{
            ////    //move rotation point to center of image
            ////    g.TranslateTransform((float)b.Width / 2, (float)b.Height / 2);
            ////    //rotate
            ////    g.RotateTransform(angle);
            ////    //move image back
            ////    g.TranslateTransform(-(float)b.Width / 2, -(float)b.Height / 2);
            ////    //draw passed in image onto graphics object
            ////    g.DrawImage(b, new Point(0, 0));
            ////}
            ////return returnBitmap;
            //if (!keepWholeImg)
            //{
            //    var bitmap = new Bitmap(image.Width, image.Height);
            //    using (var g = Graphics.FromImage(bitmap))
            //    {
            //        float hw = bitmap.Width / 2f;
            //        float hh = bitmap.Height / 2f;
            //        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            //        //translate center
            //        g.TranslateTransform(hw, hh);
            //        //rotate
            //        g.RotateTransform(angleDegrees);
            //        //re-translate
            //        g.TranslateTransform(-hw, -hh);
            //        g.DrawImage(image, 0, 0);
            //        return bitmap;
            //    }
            //}
            //else
            //{
            //    //get the new size and create the blank bitmap
            //    float rad = (float)(angleDegrees / 180.0 * Math.PI);
            //    double fW = Math.Abs((Math.Cos(rad) * image.Width)) + Math.Abs((Math.Sin(rad) * image.Height));
            //    double fH = Math.Abs((Math.Sin(rad) * image.Width)) + Math.Abs((Math.Cos(rad) * image.Height));

            //    var bitmap = new Bitmap((int)Math.Ceiling(fW), (int)Math.Ceiling(fH));
            //    using (var g = Graphics.FromImage(bitmap))
            //    {
            //        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            //        float hw = bitmap.Width / 2f;
            //        float hh = bitmap.Height / 2f;

            //        System.Drawing.Drawing2D.Matrix m = g.Transform;

            //        //here we do not need to translate, we rotate at the specified point
            //        m.RotateAt(angleDegrees, new PointF((float)(bitmap.Width / 2), (float)(bitmap.Height / 2)), System.Drawing.Drawing2D.MatrixOrder.Append);

            //        g.Transform = m;

            //        //draw the rotated image
            //        g.DrawImage(image, new PointF((float)((bitmap.Width - image.Width) / 2), (float)((bitmap.Height - image.Height) / 2)));
            //        return bitmap;
            //    }
            //}
        }

        public static Bitmap GetSquare(Bitmap image)
        {
            int length = Math.Max(image.Width, image.Height);
            var bitmap = new Bitmap(length, length);
            using var graphics = Graphics.FromImage(bitmap);
            //graphics.TranslateTransform((float)bitmap.Width / 2, (float)bitmap.Height / 2);
            //graphics.RotateTransform(angleDegrees);
            //graphics.TranslateTransform(-(float)bitmap.Width / 2, -(float)bitmap.Height / 2);
            graphics.DrawImage(image,
                bitmap.Width / 2 - image.Width / 2,
                bitmap.Height / 2 - image.Height / 2,
                image.Width, image.Height);
            return bitmap;
        }

        /// <summary>
        /// Create a sprite sheet that contains the given images arranged in a grid of cells.
        /// </summary>
        /// <param name="sheetSize">Size of the returned sprite sheet.</param>
        /// <param name="cellSize">Size of each individual image cell.</param>
        /// <param name="files">Image files that will compose the sprite sheet.</param>
        /// <returns></returns>
        public static Bitmap CreateSpriteSheet(Size sheetSize, Size cellSize, IEnumerable<FileInfo> files)
        {
            int colCount = sheetSize.Width / cellSize.Width;
            int rowCount = sheetSize.Height / cellSize.Height;

            var sheet = new Bitmap(sheetSize.Width, sheetSize.Height);
            using var graphics = Graphics.FromImage(sheet);
            int col = 0, row = 0;
            foreach (var file in files)
            {
                if (row == rowCount)
                {
                    throw new InvalidOperationException($"Number of images '{row * colCount + col}' exceeded number of cells '{colCount * rowCount}'.");
                }
                using (var image = Bitmap.FromFile(file.FullName))
                { graphics.DrawImage(image, new Point(col * cellSize.Width, row * cellSize.Height)); }
                col++;
                if (col == colCount)
                {
                    col = 0;
                    row++;
                }
            }
            return sheet;
        }
    }
}
