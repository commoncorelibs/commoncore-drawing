#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Drawing;
using System.Drawing.Imaging;

namespace CommonCore.Drawing
{
    /// <summary>
    /// The <see cref="BitmapExtensions"/> <c>static</c> <c>extension</c> <c>class</c> provides
    /// extension methods related to <see cref="Bitmap"/> objects.
    /// </summary>
    public static class BitmapExtensions
    {
        /// <summary>
        /// Get bitmap as an Icon.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Bitmap as an Icon.</returns>
        public static Icon ToIcon(this Bitmap self)
            => Icon.FromHandle(self.GetHicon());

        /// <summary>
        /// Get a scaled version of the bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="size">Size to scale the bitmap.</param>
        /// <returns>Scaled version of the bitmap.</returns>
        public static Bitmap Scale(this Bitmap self, Size size)
            => BitmapHelper.GetScaled(self, size);

        /// <summary>
        /// Get version of the bitmap with given colors swapped.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="colors">List of color pairs to swap.</param>
        /// <returns>Version of the bitmap with given colors swapped.</returns>
        public static Bitmap Swap(this Bitmap self, params ColorPair[] colors)
            => BitmapHelper.GetSwapped(self, colors);

        /// <summary>
        /// Get a cropped version of the given bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="x">X position to start the new cropped image.</param>
        /// <param name="y">Y position to start the new cropped image.</param>
        /// <param name="width">Width of the new cropped image.</param>
        /// <param name="height">Height of the new cropped image.</param>
        /// <returns>Cropped version of the given bitmap.</returns>
        public static Bitmap Crop(this Bitmap self, int x, int y, int width, int height)
            => BitmapHelper.GetCropped(self, x, y, width, height);

        /// <summary>
        /// Get bitmap with color matrix applied.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="matrix">Color matrix to apply.</param>
        /// <returns>Bitmap with color matrix applied.</returns>
        public static Bitmap Adjust(this Bitmap self, ColorMatrix matrix)
            => BitmapHelper.GetAdjusted(self, matrix);

        /// <summary>
        /// Get a grayscale version of the bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Grayscale version of the bitmap.</returns>
        public static Bitmap Grayscale(this Bitmap self)
            => BitmapHelper.GetAdjusted(self, ColorMatrixes.Grayscale);

        /// <summary>
        /// Get a negative version of the bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Negative version of the bitmap.</returns>
        public static Bitmap Negative(this Bitmap self)
            => BitmapHelper.GetAdjusted(self, ColorMatrixes.Negative);

        /// <summary>
        /// Get a sepia version of the bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Sepia version of the bitmap.</returns>
        public static Bitmap Sepia(this Bitmap self)
            => BitmapHelper.GetAdjusted(self, ColorMatrixes.Sepia);

        /// <summary>
        /// Get a shadow version of the bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Shadow version of the bitmap.</returns>
        public static Bitmap Shadow(this Bitmap self)
            => BitmapHelper.GetAdjusted(self, ColorMatrixes.Shadow);

        /// <summary>
        /// Get a tinted version of the bitmap.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="color">Color to use for tinting.</param>
        /// <param name="intensity">Intensity of tinting.</param>
        /// <returns>Tinted version of the bitmap.</returns>
        public static Bitmap Tint(this Bitmap self, Color color, float intensity = 1.0F)
            => BitmapHelper.GetTint(self, color, intensity);

        /// <summary>
        /// Get a version of the bitmap with a changed transparency.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="intensity">Intensity of transparency.</param>
        /// <returns>Version of the bitmap with a changed transparency.</returns>
        public static Bitmap Transparency(this Bitmap self, float intensity)
            => BitmapHelper.GetTransparency(self, intensity);

        /// <summary>
        /// Get new bitmap containing the specified image and its reflection.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="backColor">Color to fill the background.</param>
        /// <param name="reflectivity">Amount to reflect.</param>
        /// <returns>Bitmap with the bitmap and its reflection.</returns>
        public static Bitmap Reflection(this Bitmap self, Color backColor, int reflectivity)
            => BitmapHelper.GetReflection(self, backColor, reflectivity);

        public static Bitmap Rotate(this Bitmap self, float angleDegrees)
            => BitmapHelper.GetRotated(self, angleDegrees);

        public static Bitmap Square(this Bitmap self)
            => BitmapHelper.GetSquare(self);
    }
}
