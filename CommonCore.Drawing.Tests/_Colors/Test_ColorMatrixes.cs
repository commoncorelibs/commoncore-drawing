﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests._Colors
{
    [ExcludeFromCodeCoverage]
    public class Test_ColorMatrixes
    {
        [Fact]
        public void Test()
        {
            ColorMatrixes.Grayscale.Should().NotBeNull();
            ColorMatrixes.Negative.Should().NotBeNull();
            ColorMatrixes.Sepia.Should().NotBeNull();
            ColorMatrixes.Shadow.Should().NotBeNull();
        }
    }
}
