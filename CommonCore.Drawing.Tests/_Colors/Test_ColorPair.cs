﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests._Colors
{
    [ExcludeFromCodeCoverage]
    public class Test_ColorPair
    {
        [Fact]
        public void Test()
        {
            var colorA = Color.Red;
            var colorB = Color.Blue;
            var subject = new ColorPair(colorA, colorB);

            subject.A.Should().Be(colorA);
            subject.B.Should().Be(colorB);

            var swapped = new ColorPair(colorB, colorA);
            subject.ToSwapped().Should().Be(swapped);

            (subject == new ColorPair(colorA, colorB)).Should().BeTrue();
            (subject != new ColorPair(colorA, colorB)).Should().BeFalse();

            (subject == swapped).Should().BeFalse();
            (subject != swapped).Should().BeTrue();

            subject.Equals(new object()).Should().BeFalse();
            subject.Equals(swapped).Should().BeFalse();
            subject.Equals(subject).Should().BeTrue();

            subject.ToString().Should().NotBeNullOrEmpty();
        }
    }
}
