﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests._Colors
{
    [ExcludeFromCodeCoverage]
    public class Test_ColorExtensions
    {
        [Fact]
        public void Deconstruct_RGB()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            (byte r, byte g, byte b) = subject;
            r.Should().Be(subject.R);
            g.Should().Be(subject.G);
            b.Should().Be(subject.B);
        }

        [Fact]
        public void Deconstruct_ARGB()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            (byte a, byte r, byte g, byte b) = subject;
            a.Should().Be(subject.A);
            r.Should().Be(subject.R);
            g.Should().Be(subject.G);
            b.Should().Be(subject.B);
        }

        [Theory]
        [InlineData(0, false)]
        [InlineData(100, false)]
        [InlineData(255, true)]
        public void IsOpaque(byte alpha, bool expected)
            => Color.FromArgb(alpha, 0, 0, 0).IsOpaque().Should().Be(expected);

        [Theory]
        [InlineData(0, true)]
        [InlineData(100, false)]
        [InlineData(255, false)]
        public void IsInvisible(byte alpha, bool expected)
            => Color.FromArgb(alpha, 0, 0, 0).IsInvisible().Should().Be(expected);

        [Theory]
        [InlineData(0, true)]
        [InlineData(100, true)]
        [InlineData(255, false)]
        public void IsTransparent(byte alpha, bool expected)
            => Color.FromArgb(alpha, 0, 0, 0).IsTransparent().Should().Be(expected);

        [Theory]
        [InlineData(0, false)]
        [InlineData(100, true)]
        [InlineData(255, false)]
        public void IsTranslucent(byte alpha, bool expected)
            => Color.FromArgb(alpha, 0, 0, 0).IsTranslucent().Should().Be(expected);

        [Fact]
        public void ToBitmap()
        {
            var color = Color.FromArgb(255, 255, 0, 0);
            using var bitmap = color.ToBitmap(2, 2);
            bitmap.GetPixel(0, 0).Should().Be(color);
            bitmap.GetPixel(0, 1).Should().Be(color);
            bitmap.GetPixel(1, 0).Should().Be(color);
            bitmap.GetPixel(1, 1).Should().Be(color);

            Action act;
            
            act = () => color.ToBitmap(0, 1);
            act.Should().Throw<ArgumentOutOfRangeException>();

            act = () => color.ToBitmap(1, 0);
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Theory]
        [InlineData(0, 0, 0, 0, null, "#00000000")]
        [InlineData(1, 0, 0, 0, null, "#01000000")]
        [InlineData(0, 1, 0, 0, null, "#00010000")]
        [InlineData(0, 0, 1, 0, null, "#00000100")]
        [InlineData(0, 0, 0, 1, null, "#00000001")]
        [InlineData(255, 0, 0, 0, null, "#FF000000")]
        [InlineData(0, 255, 0, 0, null, "#00FF0000")]
        [InlineData(0, 0, 255, 0, null, "#0000FF00")]
        [InlineData(0, 0, 0, 255, null, "#000000FF")]
        [InlineData(1, 2, 3, 4, "{3:X1}-{2:X1}-{1:X1}-{0:X1}", "4-3-2-1")]
        public void ToHex(int a, int r, int g, int b, string format, string expected)
            => Color.FromArgb(a, r, g, b).ToHex(format).Should().Be(expected);

        [Fact]
        public void ToInverted()
        {
            var subject = Color.FromArgb(100, 10, 20, 30);
            var expected = Color.FromArgb(100, 245, 235, 225);
            subject.ToInverted().Should().Be(expected);
            subject.ToInverted(false).Should().Be(expected);
            subject.ToInverted(true).Should().Be(Color.FromArgb(155, expected));
        }

        [Fact]
        public void ToDesaturated()
        {
            var subject = Color.FromArgb(100, 10, 20, 30);
            subject.ToDesaturated().A.Should().Be(subject.A);
        }

        [Fact]
        public void ToBrightnessChanged()
        {
            var subject = Color.Red;
            subject.ToBrightnessChanged(0).Should().Be(subject);
            subject.ToBrightnessChanged(0.5).A.Should().Be(subject.A);
            subject.ToBrightnessChanged(-0.5).A.Should().Be(subject.A);

            Action act;

            act = () => subject.ToBrightnessChanged(-1.1);
            act.Should().Throw<ArgumentOutOfRangeException>();

            act = () => subject.ToBrightnessChanged(1.1);
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void ToLightened()
        {
            var subject = Color.Red;
            subject.ToLightened(0).Should().Be(subject);
            subject.ToLightened(0.5).A.Should().Be(subject.A);

            Action act;

            act = () => subject.ToLightened(-0.1);
            act.Should().Throw<ArgumentOutOfRangeException>();

            act = () => subject.ToLightened(1.1);
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void ToDarkened()
        {
            var subject = Color.Red;
            subject.ToDarkened(0).Should().Be(subject);
            subject.ToDarkened(0.5).A.Should().Be(subject.A);

            Action act;

            act = () => subject.ToDarkened(-0.1);
            act.Should().Throw<ArgumentOutOfRangeException>();

            act = () => subject.ToDarkened(1.1);
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void WithA()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(10, 2, 3, 4);
            subject.WithA((byte) 10).Should().Be(expected);
            subject.WithA(10).Should().Be(expected);
            subject.WithA(Color.FromArgb(10, 0, 0, 0)).Should().Be(expected);
        }

        [Fact]
        public void WithR()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(1, 10, 3, 4);
            subject.WithR((byte) 10).Should().Be(expected);
            subject.WithR(10).Should().Be(expected);
            subject.WithR(Color.FromArgb(0, 10, 0, 0)).Should().Be(expected);
        }

        [Fact]
        public void WithG()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(1, 2, 10, 4);
            subject.WithG((byte) 10).Should().Be(expected);
            subject.WithG(10).Should().Be(expected);
            subject.WithG(Color.FromArgb(0, 0, 10, 0)).Should().Be(expected);
        }

        [Fact]
        public void WithB()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(1, 2, 3, 10);
            subject.WithB((byte) 10).Should().Be(expected);
            subject.WithB(10).Should().Be(expected);
            subject.WithB(Color.FromArgb(0, 0, 0, 10)).Should().Be(expected);
        }

        [Fact]
        public void WithDelta_RGB()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(1, 12, 13, 14);
            subject.WithDelta(10, 10, 10).Should().Be(expected);
        }

        [Fact]
        public void WithDelta_ARGB()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(11, 12, 13, 14);
            subject.WithDelta(10, 10, 10, 10).Should().Be(expected);
        }

        [Fact]
        public void WithDeltaA()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(11, 2, 3, 4);
            subject.WithDeltaA(10).Should().Be(expected);
        }

        [Fact]
        public void WithDeltaR()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(1, 12, 3, 4);
            subject.WithDeltaR(10).Should().Be(expected);
        }

        [Fact]
        public void WithDeltaG()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(1, 2, 13, 4);
            subject.WithDeltaG(10).Should().Be(expected);
        }

        [Fact]
        public void WithDeltaB()
        {
            var subject = Color.FromArgb(1, 2, 3, 4);
            var expected = Color.FromArgb(1, 2, 3, 14);
            subject.WithDeltaB(10).Should().Be(expected);
        }
    }
}
