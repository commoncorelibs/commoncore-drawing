﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests._Colors
{
    [ExcludeFromCodeCoverage]
    public class Test_ColorHelper
    {
        [Theory]
        [InlineData(null, false, 0, 0, 0, 0)]
        [InlineData("", false, 0, 0, 0, 0)]
        [InlineData("XXXXXXXX", false, 0, 0, 0, 0)]
        [InlineData("#00000000", true, 0, 0, 0, 0)]
        [InlineData("00000000", true, 0, 0, 0, 0)]
        [InlineData("000000", true, 0, 0, 0, 0)]
        public void TryParseHex(string subject, bool result, int a, int r, int g, int b)
        {
            ColorHelper.TryParseHex(subject, out Color color).Should().Be(result);
            if (result) { color.Should().Be(Color.FromArgb(a, r, g, b)); }
        }
    }
}
