﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_RectangleFExtensions
    {
        [Fact]
        public void ToRect()
        {
            var subject = new RectangleF(1.5f, 2.5f, 3.5f, 4.5f);
            var expected = new Rectangle(1, 2, 3, 4);
            subject.ToRect().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToCeiling()
        {
            var subject = new RectangleF(1.5f, 2.5f, 3.5f, 4.5f);
            var expected = new Rectangle(2, 3, 4, 5);
            subject.ToCeiling().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToFloor()
        {
            var subject = new RectangleF(1.5f, 2.5f, 3.5f, 4.5f);
            var expected = new Rectangle(1, 2, 3, 4);
            subject.ToFloor().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToRound()
        {
            new RectangleF(1.6f, 2.6f, 3.6f, 4.6f).ToRound().Should().BeEquivalentTo(new Rectangle(2, 3, 4, 5));
            new RectangleF(1.4f, 2.4f, 3.4f, 4.4f).ToRound().Should().BeEquivalentTo(new Rectangle(1, 2, 3, 4));
        }

        [Fact]
        public void ToCenter()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new PointF(2.5f, 4);
            subject.ToCenter().Should().BeEquivalentTo(expected);
            subject.ToCenterX().Should().Be(expected.X);
            subject.ToCenterY().Should().Be(expected.Y);
        }

        [Fact]
        public void ToLeftX()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            subject.ToLeftX().Should().Be(subject.X);
        }

        [Theory]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 1)]
        [InlineData(1, 2, 2)]
        public void ToRightX(float x, float w, float expected)
            => new RectangleF(x, 0, w, 0).ToRightX().Should().Be(expected);

        [Fact]
        public void ToTopY()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            subject.ToTopY().Should().Be(subject.Y);
        }

        [Theory]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 1)]
        [InlineData(1, 2, 2)]
        public void ToBottomY(float y, float h, float expected)
            => new RectangleF(0, y, 0, h).ToBottomY().Should().Be(expected);

        [Theory]
        [InlineData(1, 2, 3, 4, 1, 2)]
        public void ToLeftTop(float x, float y, float w, float h, float xExpected, float yExpected)
            => new RectangleF(x, y, w, h).ToLeftTop().Should().Be(new PointF(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 1, 4)]
        public void ToLeftCenter(float x, float y, float w, float h, float xExpected, float yExpected)
            => new RectangleF(x, y, w, h).ToLeftCenter().Should().Be(new PointF(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 1, 5)]
        public void ToLeftBottom(float x, float y, float w, float h, float xExpected, float yExpected)
            => new RectangleF(x, y, w, h).ToLeftBottom().Should().Be(new PointF(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 2.5f, 2)]
        public void ToCenterTop(float x, float y, float w, float h, float xExpected, float yExpected)
            => new RectangleF(x, y, w, h).ToCenterTop().Should().Be(new PointF(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 2.5f, 5)]
        public void ToCenterBottom(float x, float y, float w, float h, float xExpected, float yExpected)
            => new RectangleF(x, y, w, h).ToCenterBottom().Should().Be(new PointF(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 3, 2)]
        public void ToRightTop(float x, float y, float w, float h, float xExpected, float yExpected)
            => new RectangleF(x, y, w, h).ToRightTop().Should().Be(new PointF(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 3, 4)]
        public void ToRightCenter(float x, float y, float w, float h, float xExpected, float yExpected)
            => new RectangleF(x, y, w, h).ToRightCenter().Should().Be(new PointF(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 3, 5)]
        public void ToRightBottom(float x, float y, float w, float h, float xExpected, float yExpected)
            => new RectangleF(x, y, w, h).ToRightBottom().Should().Be(new PointF(xExpected, yExpected));

        [Fact]
        public void ToInflated()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            subject.ToInflated(1, 0).Should().BeEquivalentTo(new RectangleF(0, 2, 5, 4));
            subject.ToInflated(0, 1).Should().BeEquivalentTo(new RectangleF(1, 1, 3, 6));
        }

        [Fact]
        public void ToDeflated()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            subject.ToDeflated(1, 0).Should().BeEquivalentTo(new RectangleF(2, 2, 1, 4));
            subject.ToDeflated(0, 1).Should().BeEquivalentTo(new RectangleF(1, 3, 3, 2));
        }

        [Fact]
        public void WithPoint()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(10, 20, 3, 4);
            subject.WithPoint(10, 20).Should().BeEquivalentTo(expected);
            subject.WithPoint(new Point(10, 20)).Should().BeEquivalentTo(expected);
            subject.WithPoint(new PointF(10, 20)).Should().BeEquivalentTo(expected);
            subject.WithPoint(new Rectangle(10, 20, 0, 0)).Should().BeEquivalentTo(expected);
            subject.WithPoint(new RectangleF(10, 20, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithX()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(10, 2, 3, 4);
            subject.WithX(10).Should().BeEquivalentTo(expected);
            subject.WithX(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithX(new PointF(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithX(new Rectangle(10, 0, 0, 0)).Should().BeEquivalentTo(expected);
            subject.WithX(new RectangleF(10, 0, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithY()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(1, 10, 3, 4);
            subject.WithY(10).Should().BeEquivalentTo(expected);
            subject.WithY(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithY(new PointF(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithY(new Rectangle(0, 10, 0, 0)).Should().BeEquivalentTo(expected);
            subject.WithY(new RectangleF(0, 10, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithSize()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(1, 2, 30, 40);
            subject.WithSize(30, 40).Should().BeEquivalentTo(expected);
            subject.WithSize(new Size(30, 40)).Should().BeEquivalentTo(expected);
            subject.WithSize(new SizeF(30, 40)).Should().BeEquivalentTo(expected);
            subject.WithSize(new Rectangle(0, 0, 30, 40)).Should().BeEquivalentTo(expected);
            subject.WithSize(new RectangleF(0, 0, 30, 40)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithWidth()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(1, 2, 10, 4);
            subject.WithWidth(10).Should().BeEquivalentTo(expected);
            subject.WithWidth(new Size(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithWidth(new SizeF(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithWidth(new Rectangle(0, 0, 10, 0)).Should().BeEquivalentTo(expected);
            subject.WithWidth(new RectangleF(0, 0, 10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithHeight()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(1, 2, 3, 10);
            subject.WithHeight(10).Should().BeEquivalentTo(expected);
            subject.WithHeight(new Size(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithHeight(new SizeF(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithHeight(new Rectangle(0, 0, 0, 10)).Should().BeEquivalentTo(expected);
            subject.WithHeight(new RectangleF(0, 0, 0, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDelta()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(11, 12, 13, 14);
            subject.WithDelta(10, 10, 10, 10).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Point(10, 10), new Size(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new PointF(10, 10), new Size(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Rectangle(10, 10, 10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new RectangleF(10, 10, 10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaPoint()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(11, 12, 3, 4);
            subject.WithDeltaPoint(10, 10).Should().BeEquivalentTo(expected);
            subject.WithDeltaPoint(new Point(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaPoint(new PointF(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaPoint(new Size(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaPoint(new SizeF(10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaX()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(11, 2, 3, 4);
            subject.WithDeltaX(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new PointF(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new Size(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new SizeF(10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaY()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(1, 12, 3, 4);
            subject.WithDeltaY(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new PointF(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new Size(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new SizeF(0, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaSize()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(1, 2, 13, 14);
            subject.WithDeltaSize(10, 10).Should().BeEquivalentTo(expected);
            subject.WithDeltaSize(new Point(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaSize(new PointF(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaSize(new Size(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaSize(new SizeF(10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaWidth()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(1, 2, 13, 4);
            subject.WithDeltaWidth(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new PointF(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new Size(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new SizeF(10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaHeight()
        {
            var subject = new RectangleF(1, 2, 3, 4);
            var expected = new RectangleF(1, 2, 3, 14);
            subject.WithDeltaHeight(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new PointF(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new Size(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new SizeF(0, 10)).Should().BeEquivalentTo(expected);
        }
    }
}
