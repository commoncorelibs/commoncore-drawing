﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_PointFExtensions
    {
        [Fact]
        public void ToPoint()
        {
            var subject = new PointF(1.5f, 2.5f);
            var expected = new Point(1, 2);
            subject.ToPoint().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToCeiling()
        {
            var subject = new PointF(1.5f, 2.5f);
            var expected = new Point(2, 3);
            subject.ToCeiling().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToFloor()
        {
            var subject = new PointF(1.5f, 2.5f);
            var expected = new Point(1, 2);
            subject.ToFloor().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToRound()
        {
            new PointF(1.6f, 2.6f).ToRound().Should().BeEquivalentTo(new Point(2, 3));
            new PointF(1.4f, 2.4f).ToRound().Should().BeEquivalentTo(new Point(1, 2));
        }

        [Fact]
        public void WithX()
        {
            var subject = new PointF(1, 2);
            var expected = new PointF(10, 2);
            subject.WithX(10).Should().BeEquivalentTo(expected);
            subject.WithX(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithX(new PointF(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithX(new Rectangle(10, 0, 0, 0)).Should().BeEquivalentTo(expected);
            subject.WithX(new RectangleF(10, 0, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithY()
        {
            var subject = new PointF(1, 2);
            var expected = new PointF(1, 10);
            subject.WithY(10).Should().BeEquivalentTo(expected);
            subject.WithY(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithY(new PointF(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithY(new Rectangle(0, 10, 0, 0)).Should().BeEquivalentTo(expected);
            subject.WithY(new RectangleF(0, 10, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDelta()
        {
            var subject = new PointF(1, 2);
            var expected = new PointF(11, 12);
            subject.WithDelta(10, 10).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Point(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new PointF(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Size(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new SizeF(10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaX()
        {
            var subject = new PointF(1, 2);
            var expected = new PointF(11, 2);
            subject.WithDeltaX(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new PointF(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new Size(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new SizeF(10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaY()
        {
            var subject = new PointF(1, 2);
            var expected = new PointF(1, 12);
            subject.WithDeltaY(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new PointF(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new Size(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new SizeF(0, 10)).Should().BeEquivalentTo(expected);
        }
    }
}
