﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_SizeFExtensions
    {
        [Fact]
        public void ToCeiling()
        {
            var subject = new SizeF(1.5f, 2.5f);
            var expected = new Size(2, 3);
            subject.ToCeiling().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToFloor()
        {
            var subject = new SizeF(1.5f, 2.5f);
            var expected = new Size(1, 2);
            subject.ToFloor().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToRound()
        {
            new SizeF(1.6f, 2.6f).ToRound().Should().BeEquivalentTo(new Size(2, 3));
            new SizeF(1.4f, 2.4f).ToRound().Should().BeEquivalentTo(new Size(1, 2));
        }

        [Fact]
        public void WithWidth()
        {
            var subject = new SizeF(3, 4);
            var expected = new SizeF(10, 4);
            subject.WithWidth(10).Should().BeEquivalentTo(expected);
            subject.WithWidth(new Size(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithWidth(new SizeF(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithWidth(new Rectangle(0, 0, 10, 0)).Should().BeEquivalentTo(expected);
            subject.WithWidth(new RectangleF(0, 0, 10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithHeight()
        {
            var subject = new SizeF(3, 4);
            var expected = new SizeF(3, 10);
            subject.WithHeight(10).Should().BeEquivalentTo(expected);
            subject.WithHeight(new Size(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithHeight(new SizeF(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithHeight(new Rectangle(0, 0, 0, 10)).Should().BeEquivalentTo(expected);
            subject.WithHeight(new RectangleF(0, 0, 0, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDelta()
        {
            var subject = new SizeF(3, 4);
            var expected = new SizeF(13, 14);
            subject.WithDelta(10, 10).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Point(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new PointF(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Size(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new SizeF(10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaWidth()
        {
            var subject = new SizeF(3, 4);
            var expected = new SizeF(13, 4);
            subject.WithDeltaWidth(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new PointF(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new Size(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new SizeF(10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaHeight()
        {
            var subject = new SizeF(3, 4);
            var expected = new SizeF(3, 14);
            subject.WithDeltaHeight(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new PointF(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new Size(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new SizeF(0, 10)).Should().BeEquivalentTo(expected);
        }
    }
}
