﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_RectangleExtensions
    {
        [Fact]
        public void ToRectF()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new RectangleF(1, 2, 3, 4);
            subject.ToRectF().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void ToCenter()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Point(2, 4);
            subject.ToCenter().Should().BeEquivalentTo(expected);
            subject.ToCenterX().Should().Be(expected.X);
            subject.ToCenterY().Should().Be(expected.Y);
        }

        [Fact]
        public void ToCenterF()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new PointF(2.5f, 4);
            subject.ToCenterF().Should().BeEquivalentTo(expected);
            subject.ToCenterXF().Should().Be(expected.X);
            subject.ToCenterYF().Should().Be(expected.Y);
        }

        [Fact]
        public void ToLeftX()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            subject.ToLeftX().Should().Be(subject.X);
        }

        [Theory]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 1)]
        [InlineData(1, 2, 2)]
        public void ToRightX(int x, int w, int expected)
            => new Rectangle(x, 0, w, 0).ToRightX().Should().Be(expected);

        [Fact]
        public void ToTopY()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            subject.ToTopY().Should().Be(subject.Y);
        }

        [Theory]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 1)]
        [InlineData(1, 2, 2)]
        public void ToBottomY(int y, int h, int expected)
            => new Rectangle(0, y, 0, h).ToBottomY().Should().Be(expected);

        [Theory]
        [InlineData(1, 2, 3, 4, 1, 2)]
        public void ToLeftTop(int x, int y, int w, int h, int xExpected, int yExpected)
            => new Rectangle(x, y, w, h).ToLeftTop().Should().Be(new Point(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 1, 4)]
        public void ToLeftCenter(int x, int y, int w, int h, int xExpected, int yExpected)
            => new Rectangle(x, y, w, h).ToLeftCenter().Should().Be(new Point(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 1, 5)]
        public void ToLeftBottom(int x, int y, int w, int h, int xExpected, int yExpected)
            => new Rectangle(x, y, w, h).ToLeftBottom().Should().Be(new Point(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 2, 2)]
        public void ToCenterTop(int x, int y, int w, int h, int xExpected, int yExpected)
            => new Rectangle(x, y, w, h).ToCenterTop().Should().Be(new Point(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 2, 5)]
        public void ToCenterBottom(int x, int y, int w, int h, int xExpected, int yExpected)
            => new Rectangle(x, y, w, h).ToCenterBottom().Should().Be(new Point(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 3, 2)]
        public void ToRightTop(int x, int y, int w, int h, int xExpected, int yExpected)
            => new Rectangle(x, y, w, h).ToRightTop().Should().Be(new Point(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 3, 4)]
        public void ToRightCenter(int x, int y, int w, int h, int xExpected, int yExpected)
            => new Rectangle(x, y, w, h).ToRightCenter().Should().Be(new Point(xExpected, yExpected));

        [Theory]
        [InlineData(1, 2, 3, 4, 3, 5)]
        public void ToRightBottom(int x, int y, int w, int h, int xExpected, int yExpected)
            => new Rectangle(x, y, w, h).ToRightBottom().Should().Be(new Point(xExpected, yExpected));

        [Fact]
        public void ToInflated()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            subject.ToInflated(1, 0).Should().BeEquivalentTo(new Rectangle(0, 2, 5, 4));
            subject.ToInflated(0, 1).Should().BeEquivalentTo(new Rectangle(1, 1, 3, 6));
        }

        [Fact]
        public void ToDeflated()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            subject.ToDeflated(1, 0).Should().BeEquivalentTo(new Rectangle(2, 2, 1, 4));
            subject.ToDeflated(0, 1).Should().BeEquivalentTo(new Rectangle(1, 3, 3, 2));
        }

        [Fact]
        public void WithPoint()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(10, 20, 3, 4);
            subject.WithPoint(10, 20).Should().BeEquivalentTo(expected);
            subject.WithPoint(new Point(10, 20)).Should().BeEquivalentTo(expected);
            subject.WithPoint(new Rectangle(10, 20, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithX()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(10, 2, 3, 4);
            subject.WithX(10).Should().BeEquivalentTo(expected);
            subject.WithX(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithX(new Rectangle(10, 0, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithY()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(1, 10, 3, 4);
            subject.WithY(10).Should().BeEquivalentTo(expected);
            subject.WithY(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithY(new Rectangle(0, 10, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithSize()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(1, 2, 30, 40);
            subject.WithSize(30, 40).Should().BeEquivalentTo(expected);
            subject.WithSize(new Size(30, 40)).Should().BeEquivalentTo(expected);
            subject.WithSize(new Rectangle(0, 0, 30, 40)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithWidth()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(1, 2, 10, 4);
            subject.WithWidth(10).Should().BeEquivalentTo(expected);
            subject.WithWidth(new Size(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithWidth(new Rectangle(0, 0, 10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithHeight()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(1, 2, 3, 10);
            subject.WithHeight(10).Should().BeEquivalentTo(expected);
            subject.WithHeight(new Size(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithHeight(new Rectangle(0, 0, 0, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDelta()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(11, 12, 13, 14);
            subject.WithDelta(10, 10, 10, 10).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Point(10, 10), new Size(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Rectangle(10, 10, 10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaPoint()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(11, 12, 3, 4);
            subject.WithDeltaPoint(10, 10).Should().BeEquivalentTo(expected);
            subject.WithDeltaPoint(new Point(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaPoint(new Size(10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaX()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(11, 2, 3, 4);
            subject.WithDeltaX(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new Size(10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaY()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(1, 12, 3, 4);
            subject.WithDeltaY(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new Size(0, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaSize()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(1, 2, 13, 14);
            subject.WithDeltaSize(10, 10).Should().BeEquivalentTo(expected);
            subject.WithDeltaSize(new Point(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaSize(new Size(10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaWidth()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(1, 2, 13, 4);
            subject.WithDeltaWidth(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new Size(10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaHeight()
        {
            var subject = new Rectangle(1, 2, 3, 4);
            var expected = new Rectangle(1, 2, 3, 14);
            subject.WithDeltaHeight(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new Size(0, 10)).Should().BeEquivalentTo(expected);
        }
    }
}
