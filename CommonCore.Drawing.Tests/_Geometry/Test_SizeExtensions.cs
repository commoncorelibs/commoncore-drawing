﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_SizeExtensions
    {
        [Fact]
        public void ToSizeF()
        {
            var subject = new Size(3, 4);
            var expected = new SizeF(3, 4);
            subject.ToSizeF().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithWidth()
        {
            var subject = new Size(3, 4);
            var expected = new Size(10, 4);
            subject.WithWidth(10).Should().BeEquivalentTo(expected);
            subject.WithWidth(new Size(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithWidth(new Rectangle(0, 0, 10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithHeight()
        {
            var subject = new Size(3, 4);
            var expected = new Size(3, 10);
            subject.WithHeight(10).Should().BeEquivalentTo(expected);
            subject.WithHeight(new Size(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithHeight(new Rectangle(0, 0, 0, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDelta()
        {
            var subject = new Size(3, 4);
            var expected = new Size(13, 14);
            subject.WithDelta(10, 10).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Point(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Size(10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaWidth()
        {
            var subject = new Size(3, 4);
            var expected = new Size(13, 4);
            subject.WithDeltaWidth(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaWidth(new Size(10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaHeight()
        {
            var subject = new Size(3, 4);
            var expected = new Size(3, 14);
            subject.WithDeltaHeight(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaHeight(new Size(0, 10)).Should().BeEquivalentTo(expected);
        }
    }
}
