﻿using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Drawing.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_PointExtensions
    {
        [Fact]
        public void ToPointF()
        {
            var subject = new Point(1, 2);
            var expected = new PointF(1, 2);
            subject.ToPointF().Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithX()
        {
            var subject = new Point(1, 2);
            var expected = new Point(10, 2);
            subject.WithX(10).Should().BeEquivalentTo(expected);
            subject.WithX(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithX(new Rectangle(10, 0, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithY()
        {
            var subject = new Point(1, 2);
            var expected = new Point(1, 10);
            subject.WithY(10).Should().BeEquivalentTo(expected);
            subject.WithY(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithY(new Rectangle(0, 10, 0, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDelta()
        {
            var subject = new Point(1, 2);
            var expected = new Point(11, 12);
            subject.WithDelta(10, 10).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Point(10, 10)).Should().BeEquivalentTo(expected);
            subject.WithDelta(new Size(10, 10)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaX()
        {
            var subject = new Point(1, 2);
            var expected = new Point(11, 2);
            subject.WithDeltaX(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new Point(10, 0)).Should().BeEquivalentTo(expected);
            subject.WithDeltaX(new Size(10, 0)).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WithDeltaY()
        {
            var subject = new Point(1, 2);
            var expected = new Point(1, 12);
            subject.WithDeltaY(10).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new Point(0, 10)).Should().BeEquivalentTo(expected);
            subject.WithDeltaY(new Size(0, 10)).Should().BeEquivalentTo(expected);
        }
    }
}
